#! /bin/bash

set -e

if [ "$(uname -s)" = "Darwin" ]; then
	config_dir=~/Library/Rime
elif [ "$(uname -s)" = "Linux" ]; then
	config_dir=~/.config/fcitx/rime
	if [ -d ~/.config/fcitx5/rime ]; then
		config_dir=~/.config/fcitx5/rime
	fi
else
	echo 'Unsupported platform.'
	exit 1
fi

function get_repo() {
	local url=$1

	local tmp_dir=$(mktemp -d)
	git clone --depth 1 $url $tmp_dir
	rm_unwanted_files $tmp_dir
	echo $tmp_dir
}

function rm_unwanted_files() {
	local target_dir=$1

	for x in default.custom.yaml squirrel.custom.yaml; do
		rm -f $target_dir/$x
	done
}

echo "Updating rime-wubi86-jidian"
tmp_dir=$(get_repo https://github.com/KyleBing/rime-wubi86-jidian)
cp $tmp_dir/*.yaml $tmp_dir/*.lua $config_dir/
rm -rf $tmp_dir

echo "Updating rime-emoji"
tmp_dir=$(get_repo https://github.com/rime/rime-emoji)
mkdir -p $config_dir/opencc
cp -R $tmp_dir/opencc/* $config_dir/opencc/
rm -rf $tmp_dir
