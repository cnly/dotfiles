#! /bin/bash

# https://github.com/rime/squirrel/issues/375

set -e

cd '/Library/Input Methods/Squirrel.app/Contents'
sudo /usr/libexec/PlistBuddy -c 'Add :TICapsLockLanguageSwitchCapable bool true' Info.plist

/usr/libexec/PlistBuddy -c 'print' Info.plist | grep TICapsLockLanguageSwitchCapable
