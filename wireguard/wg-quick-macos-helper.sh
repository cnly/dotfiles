#! /bin/bash

# This helps `wg-quick` to:
# 1. Use the tun name specified in the command on macOS
# 2. Set a route for the endpoint(s) by setting $AUTO_ROUTE{4,6}=1
# 3. Better find homebrew's bash by modifying $PATH
# Use like: $0 <up|down> <TUN NAME>

if [ "$(uname -s)" != "Darwin" ]; then
	echo 'Warning: This is probably pointless on platforms other than macOS.'
fi

set -e

act_as_wg_userspace_impl() {
	# 1. Extract the interface name from $WG_TUN_NAME_FILE
	# 2. Run wireguard-go with that exact name
	# 3. Write the same name to $WG_TUN_NAME_FILE

	wg_iface_name=$1
	tun_namefile_base=$(basename "$WG_TUN_NAME_FILE")
	wg_iface_name=${tun_namefile_base%.*}

	wireguard-go "$wg_iface_name"

	echo "$wg_iface_name" > "$WG_TUN_NAME_FILE"
}

act_as_entrypoint() {
	# macOS's bash may be too old for wg-quick
	export PATH=/usr/local/bin:$PATH
	sudo env WG_QUICK_USERSPACE_IMPLEMENTATION=$(realpath "$0") \
		AUTO_ROUTE4=1 \
		AUTO_ROUTE6=1 \
		wg-quick $@
}

if [ -n "$WG_TUN_NAME_FILE" ]; then
	# I'm being invoked by wg-quick
	act_as_wg_userspace_impl $@
else
	if [ $# != 2 ]; then
		echo "Usage: $0 <up|down> <TUN NAME>"
		exit 1
	fi
	act_as_entrypoint $@
fi
