#! /bin/bash

# TODO: Support IPv6

get_dns_record() {
	local type=$1
	local hostname=$2
	dig $1 "$hostname" +trace +short | tail -n1 | grep '^A' | cut -d' ' -f2
}

set -euo pipefail

peer_pubkey=$1
endpoint_hostname=$2

old_endpoint="$(wg show wg0 endpoints | grep "$peer_pubkey" | awk '{print $2}')"

new_addr=$(get_dns_record a "$endpoint_hostname")

new_endpoint="$new_addr:$(echo $old_endpoint | awk -F: '{print $NF}')"
if [[ ! "$new_endpoint" =~ ^([0-9]{1,3}\.){3}[0-9]{1,3}(:.+)?$ ]]; then
	echo "New endpoint '$new_enpoint' is not a valid endpoint. Aborting."
	exit 1
fi

if [ "$new_endpoint" != "$old_endpoint" ]; then
	echo "Updating peer endpoint..."
	wg set wg0 peer "$peer_pubkey" endpoint "$new_endpoint"
fi
