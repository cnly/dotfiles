local has, tree = pcall(require, 'nvim-tree')
if has then
  -- disable netrw at the very start of your init.lua (strongly advised)
  vim.g.loaded_netrw = 1
  vim.g.loaded_netrwPlugin = 1

  -- set termguicolors to enable highlight groups
  vim.opt.termguicolors = true

  local function on_attach(bufnr)
    local api = require('nvim-tree.api')

    local function opts(desc)
      return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
    end

    api.config.mappings.default_on_attach(bufnr)
    vim.keymap.del('n', '<C-x>', { buffer = bufnr })
    vim.keymap.del('n', '<C-e>', { buffer = bufnr })
    vim.keymap.del('n', ']e', { buffer = bufnr })
    vim.keymap.del('n', '[e', { buffer = bufnr })
    vim.keymap.set('n', 'Q', api.node.show_info_popup, opts('Info'))
    vim.keymap.set('n', '<C-s>', api.node.open.horizontal, opts('Open: Horizontal Split'))
    vim.keymap.set('n', ']g', api.node.navigate.diagnostics.next, opts('Next Diagnostic'))
    vim.keymap.set('n', '[g', api.node.navigate.diagnostics.prev, opts('Prev Diagnostic'))
  end

  tree.setup({
    on_attach = on_attach,
    sort_by = "case_sensitive",
    respect_buf_cwd = true,
    renderer = {
      group_empty = true,
    },
    filters = {
      dotfiles = true,
    },
    view = {
      width = 35,
    },
    actions = {
      open_file = {
        window_picker = {
          enable = false,
        },
      },
    },
  })
end

local has, tscfg = pcall(require, 'nvim-treesitter.configs')
if has then
  tscfg.setup {
    ensure_installed = {
      "asm", "bash", "bibtex", "c", "clojure", "cmake", "comment",
      "commonlisp", "cpp", "css", "csv", "cuda", "devicetree", "dockerfile",
      "dot", "gitattributes", "glsl", "go", "gomod", "gosum", "graphql", "gpg",
      "html", "http", "java", "javascript", "jq", "json", "jsonc", "julia",
      "kconfig", "kotlin", "latex", "llvm", "lua", "luadoc", "make", "mlir",
      "ninja", "nix", "objc", "objdump", "org", "perl", "php", "properties",
      "proto", "python", "query", "r", "regex", "ruby", "rust", "scala", "scss",
      "sql", "strace", "terraform", "tmux", "toml", "tsx", "typescript", "vim",
      "vimdoc", "vue", "yaml",
    },
    ignore_install = {}, -- List of parsers to ignore installing
    highlight = {
      enable = true,              -- false will disable the whole extension
      disable = { 'latex' },  -- latex handled by vimtex
      -- Workaround for: Python: [ in string causes indent on following lines https://github.com/nvim-treesitter/nvim-treesitter/issues/1573
      -- additional_vim_regex_highlighting = true
      additional_vim_regex_highlighting = { 'python' }
    },
    indent = {
      enable = false
    },
    playground = {
      enable = true,
      disable = {},
      updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
      persist_queries = false, -- Whether the query persists across vim sessions
      keybindings = {
        toggle_query_editor = 'o',
        toggle_hl_groups = 'i',
        toggle_injected_languages = 't',
        toggle_anonymous_nodes = 'a',
        toggle_language_display = 'I',
        focus_language = 'f',
        unfocus_language = 'F',
        update = 'R',
        goto_node = '<cr>',
        show_help = '?',
      }
    },
    matchup = {
      enable = true,              -- mandatory, false will disable the whole extension
      -- disable = { "c", "ruby" },  -- optional, list of language that will be disabled
    },
    autotag = {
      enable = true,
    },
    textobjects = {
      select = {
        enable = true,

        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = true,

        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",
          ["ac"] = "@class.outer",
          ["ic"] = "@class.inner",
          ["ia"] = "@parameter.inner",
          ["aa"] = "@parameter.outer",
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          ["]m"] = "@function.outer",
          ["]]"] = "@class.outer",
          ["]a"] = "@parameter.inner",
        },
        goto_next_end = {
          ["]M"] = "@function.outer",
          ["]["] = "@class.outer",
          ["]A"] = "@parameter.inner",
        },
        goto_previous_start = {
          ["[m"] = "@function.outer",
          ["[["] = "@class.outer",
          ["[a"] = "@parameter.inner",
        },
        goto_previous_end = {
          ["[M"] = "@function.outer",
          ["[]"] = "@class.outer",
          ["[A"] = "@parameter.inner",
        },
      },
    },
  }
end

local has, tshl = pcall(require, 'nvim-treesitter.highlight')
if has then
  local hlmap = vim.treesitter.highlighter.hl_map
  if hlmap ~= nil then
    -- Disable highlighting for syntax errors
    hlmap.error = nil
  end
end

-- require('tabline').setup{
  -- no_name = '[No Name]',    -- Name for buffers with no name
  -- modified_icon = '+',      -- Icon for showing modified buffer
  -- -- close_icon = '',      -- Icon for closing tab with mouse
  -- close_icon = '',          -- No close icon
  -- separator = "▌",          -- Separator icon on the left side
  -- padding = 0,              -- Prefix and suffix space
  -- color_all_icons = false,   -- Color devicons in active and inactive tabs
  -- always_show_tabs = false, -- Always show tabline
  -- right_separator = false,  -- Show right separator on the last tab
  -- show_index = true,        -- Shows the index of tab before filename
  -- show_icon = true,         -- Shows the devicon
-- }

local has, gitsigns = pcall(require, 'gitsigns')
if has then
  gitsigns.setup {
    -- base = 'HEAD',
    signcolumn = true,
    signs = {
      add    = { text = '┃' },
      change = { text = '┃' },
    },
    signs_staged_enable = false,
    current_line_blame_opts = {
      virt_text_pos = 'right_align', -- 'eol' | 'overlay' | 'right_align'
    },
    on_attach = function(bufnr)
      local function map(mode, lhs, rhs, opts)
          opts = vim.tbl_extend('force', {noremap = true, silent = true}, opts or {})
          vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts)
      end

      -- Navigation
      map('n', ']c', "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'", {expr=true})
      map('n', '[c', "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'", {expr=true})

      -- Actions
      map('n', '<leader>hs', '<cmd>Gitsigns stage_hunk<CR>')
      map('v', '<leader>hs', '<cmd>Gitsigns stage_hunk<CR>')
      map('n', '<leader>hr', '<cmd>Gitsigns reset_hunk<CR>')
      map('v', '<leader>hr', '<cmd>Gitsigns reset_hunk<CR>')
      map('n', '<leader>hS', '<cmd>Gitsigns stage_buffer<CR>')
      map('n', '<leader>hu', '<cmd>Gitsigns undo_stage_hunk<CR>')
      map('n', '<leader>hR', '<cmd>Gitsigns reset_buffer<CR>')
      map('n', '<leader>hp', '<cmd>Gitsigns preview_hunk<CR>')
      map('n', '<leader>hb', '<cmd>lua require"gitsigns".blame_line{full=true}<CR>')
      map('n', '<leader>tb', '<cmd>Gitsigns toggle_current_line_blame<CR>')
      map('n', '<leader>hd', '<cmd>Gitsigns diffthis<CR>')
      map('n', '<leader>hD', '<cmd>lua require"gitsigns".diffthis("~")<CR>')
      map('n', '<leader>td', '<cmd>Gitsigns toggle_deleted<CR>')

      -- Text object
      map('o', 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      map('x', 'ih', ':<C-U>Gitsigns select_hunk<CR>')
    end
  }
end

require('spectre').setup {
  find_engine = {
    ['ag'] = {
      cmd = "ag",
      args = {
        '--vimgrep',
        '-s'
      } ,
      options = {
        ['ignore-case'] = {
          value= "-i",
          icon="[I]",
          desc="ignore case"
        },
        ['hidden'] = {
          value="--hidden",
          desc="hidden file",
          icon="[H]"
        },
        ['unrestricted'] = {
          value="--unrestricted",
          desc="Unrestricted",
          icon="[u]"
        },
        ['skip-vcs-ignores'] = {
          value="--skip-vcs-ignores",
          desc="Skip VCS Ignores",
          icon="[U]"
        },
      },
    },
  },
  replace_engine={
    ['sed']={
      cmd = "sed",
      args = {
        '-E',
      }
    },
    options = {
      ['ignore-case'] = {
        value= "--ignore-case",
        icon="[I]",
        desc="ignore case"
      },
    },
  },
  default = {
    find = {
      --pick one of item in find_engine
      cmd = "ag",
      options = {"ignore-case"}
    },
  },
}

local has, ibl = pcall(require, 'ibl')
if has then  -- v3, use default config
  ibl.setup {
    indent = {
      char = "▏",
    },
  }
else
  require("indent_blankline").setup {
    show_current_context = true,
    show_current_context_start = true,
  }
end

local has, tshjkl = pcall(require, 'tshjkl')
if has then
  tshjkl.setup {
  }
end

local has, dropbar = pcall(require, 'dropbar')
if has then
  dropbar.setup {
    opts = {
      bar = {
        update_debounce = 200,
      },
    },
    sources = {
      treesitter = {
        valid_types = {
          'class',
          'enum',
          'function',
          'h1_marker',
          'h2_marker',
          'h3_marker',
          'h4_marker',
          'h5_marker',
          'h6_marker',
          'interface',
          'method',
          'module',
          'namespace',
          'package',
          'struct',
          'type',
        },
      },
    },
  }
end

local has, scrollview = pcall(require, 'scrollview')
if has then
  require('scrollview.contrib.gitsigns').setup {
    -- add_symbol = '│',
    -- change_symbol = '│',
    -- delete_symbol = '│',
  }
  require('scrollview.contrib.coc').setup {
    error_symbol = '▪',
    warn_symbol = '▪',
    -- severities = {'error', 'warn'},
    severities = {'error'},
  }
end

local has, satellite = pcall(require, 'satellite')
if has then
  satellite.setup {
    handlers = {
      cursor = {
        enable = false,
      },
    },
    diagnostic = {
      enable = true,
      signs = {'-', '=', '≡'},
      min_severity = vim.diagnostic.severity.ERROR,
      -- Highlights:
      -- - SatelliteDiagnosticError (default links to DiagnosticError)
      -- - SatelliteDiagnosticWarn (default links to DiagnosticWarn)
      -- - SatelliteDiagnosticInfo (default links to DiagnosticInfo)
      -- - SatelliteDiagnosticHint (default links to DiagnosticHint)
    },
  }
end

local has, tabby_tabline = pcall(require, 'tabby.tabline')
if has then
  local theme = {
    fill = 'TabLineFill',
    head = 'TabLine',
    current_tab = 'TabLineSel',
    -- current_tab = { fg = '#F8FBF6', bg = '#896a98', style = 'italic' },
    tab = 'TabLine',
    win = 'TabLine',
    tail = 'TabLine',
  }
  tabby_tabline.set(function(line)
    return {
      line.tabs().foreach(function(tab)
        local hl = tab.is_current() and theme.current_tab or theme.tab

        local win_is_modified = function(win)
          return vim.bo[win.buf().id].modified
        end

        local tab_is_modified = function(tab)
          local is_modified = false
          tab.wins().foreach(function(win)
            if win_is_modified(win) then
              is_modified = true
            end
          end)
          return is_modified
        end

        return {
          line.truncate_point(),
          line.sep('', hl, theme.fill),
          tab.number(),
          tab.current_win().file_icon(),
          tab.name(),
          tab_is_modified(tab) and '+' or '',
          -- tab.close_btn(''), -- show a close button
          line.sep(' ', hl, theme.fill),
          hl = hl,
          margin = ' ',
        }
      end),
    }
  end, {
    tab_name = {
      name_fallback = function(tabid)
        local api = require('tabby.module.api')
        local buf_name = require('tabby.feature.buf_name')
        local get_win_count_icon = function(win_count)
          if win_count > 9 then
            return '󰏀'
          end
          local icons = {'', ' 󰎩', ' 󰎫', ' 󰎲', ' 󰎯', ' 󰎴', ' 󰎷', ' 󰎺', ' 󰎽'}
          return icons[win_count]
        end
        local wins = api.get_tab_wins(tabid)
        local cur_win = api.get_tab_current_win(tabid)
        local name = ''
        if api.is_float_win(cur_win) then
          name = '[Floating]'
        else
          name = buf_name.get(cur_win)
        end
        name = string.format('%s%s', name, get_win_count_icon(#wins))
        return name
      end
    },
    buf_name = {
      mode = "'unique'|'relative'|'tail'|'shorten'",
    }
  })
end
