#! /bin/bash

[ -e /.dockerenv ] && IS_IN_DOCKER=1
[ "$(uname -s)" = "Darwin" ] && IS_DARWIN=1

awk_exe=${IS_DARWIN:+g}awk
grep_exe=${IS_DARWIN:+g}grep
sed_exe=${IS_DARWIN:+g}sed
tail_exe=${IS_DARWIN:+g}tail
tr_exe=${IS_DARWIN:+g}tr
cut_exe=${IS_DARWIN:+g}cut
mktemp_exe=${IS_DARWIN:+g}mktemp
realpath_exe=${IS_DARWIN:+g}realpath

USER=${USER:-$(id -nu)}

if [ -n "${IS_DARWIN:-}" ]; then
	DEFAULT_USER=$(id -nu 501 2> /dev/null || true)
	DEFAULT_BREW_PREFIX=/opt/homebrew
else
	DEFAULT_USER=$(id -nu 1000 2> /dev/null || true)
	DEFAULT_BREW_PREFIX=/home/linuxbrew/.linuxbrew
fi
DEFAULT_BREW_BIN_DIR=$DEFAULT_BREW_PREFIX/bin

function is_in_docker() {
	[ -n "${IS_IN_DOCKER:-}" ]
}

function is_darwin() {
	[ -n "${IS_DARWIN:-}" ]
}

function is_root() {
	[ "$EUID" = 0 ]
}

function platform_os_version() {
	platform=$(uname -s | tr '[:upper:]' '[:lower:]')
	if is_darwin; then
		version=$(sw_vers | grep ProductVersion | awk '{print $2}')
	else
		source /etc/os-release
		version=${ID:-}-${VERSION_ID:-}
	fi
	echo "$platform"-"$version"
}

function get_timezone() {
	if is_darwin; then
		readlink /etc/localtime | cut -d/ -f 6,7
	else
		cat /etc/timezone
	fi
}
