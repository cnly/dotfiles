#! /bin/bash

function command_exists() {
	type -p "$@" &> /dev/null
}

function commands_exist() {
	for cmd in "$@"; do
		command_exists "$cmd" || return 1
	done
	return 0
}

function command_not_exists() {
	! command_exists "$@"
}

function ensure_bb() {
	local target_dir=${1:-.}
	if command_exists bb && bb --version | grep -q babashka; then
		echo bb
		return
	fi
	bb_exec="$target_dir/bb"
	if [ ! -x "$bb_exec" ] || ! "$bb_exec" --version | grep -q babashka; then
		if command_not_exists curl; then
			sudo apt install -y curl >&2
		fi
		[ ! -f /.dockerenv ] || local static_flag=--static
		# shellcheck disable=SC2086
		bash <(curl -s https://raw.githubusercontent.com/babashka/babashka/master/install) \
			--dir "$target_dir" \
			${static_flag:-} >&2 || return 1
	fi
	echo "$bb_exec"
}

function bash_ask_yn_default_yes() {
	[ -t 0 ] || return 0
	local prompt=$1
	read -r -p "$prompt [Y/n] " -n1 choice
	case "$choice" in
		n|N)
			echo
			return 1
			;;
	esac
}

function bash_ask_yn_default_no() {
	[ -t 0 ] || return 1
	local prompt=$1
	read -r -p "$prompt [y/N] " -n1 choice
	case "$choice" in
		y|Y)
			echo
			return 0
			;;
	esac
	return 1
}

function abort() {
	echo "${1:-An error occurred. Aborting.}"
	exit "${2:-1}"
}

function remove_from_path() {
	local str=$1
	PATH=${PATH//:$str:/:}  # Equivalent to s|:$str:|:|g
	PATH=${PATH#"$str":}  # Equivalent to s|^$str:||
	PATH=${PATH%:"$str"}  # Equivalent to s|:$str\$||
}

function add_to_path() {
	local str=$1
	local prepend=${2:-}
	remove_from_path "$str"
	if [ -n "$prepend" ]; then
		PATH=$str:$PATH
	else
		PATH=$PATH:$str
	fi
}

# Proxy {{{

function set_http_proxy() {
	local proxy=$1
	local no_set_x=$2
	if [ -n "$proxy" ]; then
		proxy=http://$proxy
	fi

	[ -n "$no_set_x" ] || set -x
	export no_proxy=localhost,127.0.0.1,127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
	export NO_PROXY=localhost,127.0.0.1,127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
	export http_proxy=$proxy
	export https_proxy=$proxy
	export all_proxy=$proxy
	export HTTP_PROXY=$proxy
	export HTTPS_PROXY=$proxy
	export ALL_PROXY=$proxy
}

function set_proxy() {
	local proxy=${1:-${UP_PROXY:-127.0.0.1:8123}}

	local proxy_host=$(echo "$proxy" | cut -d: -f1)
	# TODO: macOS is not supported
	local proxy_host_ip=$(getent hosts "$proxy_host" | head -n1 | awk '{print $1}')
	[ -n "$proxy_host_ip" ] || proxy_host_ip=$proxy_host
	local proxychains_proxy=$(echo "$proxy" | sed -E "s/$proxy_host/$proxy_host_ip/g" | sed 's/:/ /g')

	echo 'Configuring proxy...'
	set_http_proxy "$proxy" __no_set_x
	git config --global http.proxy "$proxy"
	if [ -f ~/.proxychains/proxychains.conf.preamble ]; then
		cat ~/.proxychains/proxychains.conf.preamble > ~/.proxychains/proxychains.conf
		echo "http" "$proxychains_proxy" >> ~/.proxychains/proxychains.conf
	fi
	print_proxy_settings
}

function unset_proxy() {
	echo 'Deconfiguring proxy...'
	set_http_proxy "" __no_set_x
	git config --global --unset http.proxy
	print_proxy_settings
}

function print_proxy_settings() {
	local env_proxy_settings=$(env | grep --color=never -iF proxy | sed 's/^/    /')
	[ -n "$env_proxy_settings" ] && {
		echo 'Environment:'
		echo "$env_proxy_settings"
	} || echo 'Environment: (None)'
	echo 'Git:' "$(git config --global --get http.proxy || echo '(None)')"
	echo 'Proxychains:' "$(tail ~/.proxychains/proxychains.conf -n1 2> /dev/null || echo '(None)')"
}

# }}}

function use_ssh_agent() {
	local src=${1:-tmp}  # tmp, home, gpg, 1p

	local sock_path=
	if [[ "$src" =~ ^t(mp)?$ ]]; then
		for x in /tmp/ssh-*/agent.*; do
			SSH_AUTH_SOCK=$x timeout 1 ssh-add -L > /dev/null 2>&1
			local exit_code=$?
			[ $exit_code != 124 ] || continue
			echo "Possible agent found with status $exit_code: $x"
			case $exit_code in
				0)
					# 0 is already the best
					echo "We are already satisfied :)"
					sock_path=$x
					break
					;;
				1)
					sock_path=$x
					;;
				# exit code 2 is ignored
			esac
		done
		if [ -z "${sock_path}" ]; then
			echo "No suitable ssh agent found."
			return 1
		fi
	elif [[ "$src" =~ ^h(ome)?$ ]]; then
		sock_path=~/.ssh-agent.sock
		[ -e $sock_path ] || ssh-agent -a $sock_path || {
			echo "$sock_path not found and unable to start ssh-agent."
			return 1
		}
	elif [[ "$src" =~ ^g(pg)?$ ]]; then
		sock_path=$(gpgconf --list-dir agent-ssh-socket 2> /dev/null)
		[ -e $sock_path ] || gpgconf --launch gpg-agent || {
			echo "$sock_path not found and unable to start gpg-agent."
			return 1
		}
	elif [[ "$src" =~ ^1(p)?$ ]]; then
		sock_path=~/.1password/agent.sock
		if is_darwin && [ ! -e "$sock_path" ]; then
			echo "Creating soft link for the agent..."
			mkdir -p ~/.1password && \
				ln -s ~/Library/Group\ Containers/2BUA8C4S2C.com.1password/t/agent.sock \
				~/.1password/agent.sock
		fi
		[ -e "$sock_path" ] || {
			echo "$sock_path not found. Is it enabled?"
			return 1
		}
	else
		echo "Usage: $0 <src>, where src = t[mp]|h[ome]|g[pg]|1[p]"
		return 1
	fi

	echo "Using agent: $sock_path"
	export SSH_AUTH_SOCK=$sock_path
	# output of wc on mac contains spaces
	echo "The agent has $(timeout 1 ssh-add -L 2> /dev/null | wc -l | sed 's| ||g') keys."
}

function TMUXAON() {
	local target=$1
	# shellcheck disable=SC2016
	local tmux_detector='{ tmux=~/.local/bin/tmux && [ -x "$tmux" ]; } ||
		{ tmux=/home/linuxbrew/.linuxbrew/bin/tmux && [ -x "$tmux" ]; } ||
		tmux=$(which tmux)'
	tmux_detector=$(echo "$tmux_detector" | tr -d '\n' | tr '\t' ' ')
	if [ -n "$target" ]; then
		local q_target=$(printf %q "$target")
		echo "$tmux_detector; \"\$tmux\" attach -d -t $q_target || \"\$tmux\" new -s $q_target"
		return
	fi
	echo "$tmux_detector; \"\$tmux\" attach -d || \"\$tmux\" new"
}

function tmuxaon() {
	eval "$(TMUXAON "$1")"
}

function st() {
	local target=$1
	local session=$2
	[ -z "$V" ] || {
		set -x
		local verbose=-v
	}

	[ -n "$target" ] || (abort 'Usage: st <ssh-target> [tmux-session-name]') || return
	local window_name=$target
	[ -z "$session" ] || window_name+=-$session
	[ -z "$TMUX" ] || tmux rename-window -t "$TMUX_PANE" "$window_name";
	if [ -z "$MOSH" ]; then
		ssh -t $verbose -- "$target" "sh -c '$(TMUXAON "$session")'"
	else
		mosh -- "$target" sh -c "$(TMUXAON "$session")"
	fi
	# Disable mouse reporting https://superuser.com/a/892482
	printf '\e[?1000l'
	# Ensure alternate screen is exited https://stackoverflow.com/a/11024208
	printf '\e[?1049h'
	printf '\e[?1049l'
}

function mt() {
	MOSH=1 st "$@"
}

function sshloop() {
	local init_sleep_interval=1
	local max_sleep_interval=5
	local max_failures=10
	local min_good_interval=5
	local max_bad_interval=60

	local failures=0
	local sleep_interval=$init_sleep_interval
	while true; do
		local start_time=$(date +%s)
		"$@"
		if [ $? = 0 ] && (($(date +%s) - start_time > min_good_interval)) || \
			(($(date +%s) - start_time > max_bad_interval)); then
			failures=0
			sleep_interval=$init_sleep_interval
		else
			((failures+=1))
		fi
		if [ $failures -ge $max_failures ]; then
			echo "$0: Maximum failures ($max_failures) reached."
			return 1
		fi
		echo "$0: Failure $failures/$max_failures. Sleeping for ${sleep_interval}s..."
		sleep $sleep_interval
		[[ "$sleep_interval" -ge "$max_sleep_interval" ]] || ((sleep_interval+=1))
	done
}

function test_true_colours() {
	# https://unix.stackexchange.com/questions/404414/print-true-color-24-bit-test-pattern
	awk -v term_cols="${width:-$(tput cols || echo 80)}" 'BEGIN{
		s="/\\";
		for (colnum = 0; colnum<term_cols; colnum++) {
			r = 255-(colnum*255/term_cols);
			g = (colnum*510/term_cols);
			b = (colnum*255/term_cols);
			if (g>255) g = 510-g;
				printf "\033[48;2;%d;%d;%dm", r,g,b;
				printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
				printf "%s\033[0m", substr(s,colnum%2+1,1);
			}
		printf "\n";
	}'
}

function ifreup() {
	if [ "$EUID" = 0 ]; then
		ifdown --force "$@"; ifup "$@"
	else
		sudo ifdown --force "$@"; sudo ifup "$@"
	fi
}

function waitpid() {
	local pid=$1
	echo "Waiting for process: $pid"
	ps -p "$pid" -o user,pid,command || (abort "No such process.") || return
	$tail_exe -f /dev/null --pid "$pid"
}

function macvendor() {
	local query=$1
	query=$(echo "$query" | $tr_exe -dc 'a-fA-F0-9' | head -c 6)
	echo "$query:" "$(curl https://macvendors.com/query/"$query" -sS)"
}

function compdb_do() {
	local build_dir=${1:-.}
	local tmpfile=$(mktemp)
	compdb -p "$build_dir" list > "$tmpfile" && mv "$tmpfile" ./compile_commands.json
}

# vim: set fdm=marker:
