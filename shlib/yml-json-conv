#! /usr/bin/env bash

set -euo pipefail

THIS_FILE=${BASH_SOURCE[0]}
SCRIPT_DIR="$( cd "$( dirname "$THIS_FILE" )" &> /dev/null && pwd )"
source "$SCRIPT_DIR/detect-env.sh"
source "$SCRIPT_DIR/utils.sh"

function yml2json() {
	if [ $# = 0 ]; then
		cat | yq
	elif [ $# = 1 ]; then
		if [[ $1 = *.json ]]; then
			local tmpfile=$($mktemp_exe -p "$PWD" "$1.XXXXXXXXXX")
			cat | yq > "$tmpfile"
			[ -s "$tmpfile" -a "$(cat "$tmpfile")" != 'null' ] || {
				echo 'Aborted.'
				rm "$tmpfile"
				return 1
			}
			mv "$tmpfile" "$1"
		else
			yq < "$1"
		fi
	elif [ $# = 2 ]; then
		if [[ $1 = *.json ]]; then
			bash_ask_yn_default_no "Double check your argument order! Continue?" || return 1
		fi
		yq < "$1" > "$2"
	else
		echo "Usage: $0 [arg1 [arg2]]"
	fi
}

function json2yml() {
	if [ $# = 0 ]; then
		jq . | yq -y
	elif [ $# = 1 ]; then
		if [[ $1 = *.json ]]; then
			jq . "$1" | yq -y
		else
			local tmpfile=$($mktemp_exe -p "$PWD" "$1.XXXXXXXXXX")
			jq . | yq -y > "$tmpfile"
			[ -s "$tmpfile" ] || {
				echo 'Aborted.'
				rm "$tmpfile"
				return 1
			}
			mv "$tmpfile" "$1"
		fi
	elif [ $# = 2 ]; then
		if [[ $2 = *.json ]]; then
			bash_ask_yn_default_no "Double check your argument order! Continue?" || return 1
		fi
		jq . "$1" | yq -y > "$2"
	else
		echo "Usage: $0 [arg1 [arg2]]"
	fi
}

[ -L "$THIS_FILE" ] || {
	echo 'Invoke this script as yml2json or json2yml.'
	exit 1
}

$(basename "$THIS_FILE") "$@"
