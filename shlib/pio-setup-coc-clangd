#! /usr/bin/env bash

set -euo pipefail
set -x

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "$SCRIPT_DIR/utils.sh"

pio_exe=$(which pio 2> /dev/null) || pio_exe=~/.platformio/penv/bin/pio
clangd_exe=$(find ~/.espressif/tools/esp-clang -name 'clangd' | tail -n1)

[ -f "$pio_exe" ]
[ -e "$clangd_exe" ]

[ -f platformio.ini ]

compiler=$($pio_exe project metadata | grep cxx_path | awk '{print $3}' | tr -d '"')

cat > .clangd <<EOF
CompileFlags:
  Add:
    - -Wno-unknown-warning-option
  Remove: [-m*, -f*]
  Compiler: $compiler
Diagnostics:
  Suppress:
    - asm_invalid_output_constraint
EOF

mkdir -p .vim
cat > .vim/coc-settings.json <<EOF
{
  "clangd.path": "$clangd_exe",
  "clangd.arguments": ["--query-driver=$(dirname "$compiler")/*"]
}
EOF

$pio_exe run -t compiledb
