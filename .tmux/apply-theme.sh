#! /bin/bash
# shellcheck disable=SC1090,2046,2086

function interpret_colour() {
	local colour=$1
	if [ -z "$colour" ] || [ "$colour" = 'default' ]; then
		echo 'default'
	else
		echo 'colour'"$colour"
	fi
}

if [ "$(cat ~/.theme 2> /dev/null)" = "dark" ]; then
	source ~/.tmux/theme-dark.sh
else
	source ~/.tmux/theme-light.sh
fi

# Basic status bar colors
tmux set -g status-style fg=$(interpret_colour $STATUS_BAR_MAIN_FG),bg=$(interpret_colour $STATUS_BAR_MAIN_BG)

# Left side of status bar
status_left_identifiers_style="#[fg=$(interpret_colour $STATUS_LEFT_IDENTIFIERS_FG),bg=$(interpret_colour $STATUS_LEFT_BG)]#[bold]"
status_left_normal_style="#[fg=$(interpret_colour $STATUS_LEFT_NORMAL_FG),bg=$(interpret_colour $STATUS_LEFT_BG)]#[nobold]"
status_left_mouse_component='#([ "$(tmux show-options -g mouse)" = "mouse on" ] || printf "󰍾 ")'
status_left_path_component="#{?#{==:#{pane_current_path},$HOME},~,#{s|^$HOME/|~/|:pane_current_path}}"
status_left_sep_component="
	#[fg=$(interpret_colour $STATUS_LEFT_SEPARATOR_FG),bg=$(interpret_colour $STATUS_LEFT_BG)]#[nobold]
	#[fg=$(interpret_colour $STATUS_LEFT_NORMAL_FG),bg=$(interpret_colour $STATUS_LEFT_BG)]"
# ---
tmp="
	${status_left_identifiers_style} 
	#S:#I:#P 
	${status_left_mouse_component}
	${status_left_sep_component} 
	${status_left_path_component} 
	#[fg=$(interpret_colour $STATUS_LEFT_BG),bg=$(interpret_colour $STATUS_BAR_MAIN_BG)]"
tmux set -g status-left "$(echo "$tmp" | tr -d \\t)"

tmux set -g status-left-length 150

# Right side of status bar
status_right_normal_style="#[fg=$(interpret_colour $STATUS_RIGHT_NORMAL_FG),bg=$(interpret_colour $STATUS_RIGHT_BG)]#[nobold]"
status_right_sep_component="
	#[fg=$(interpret_colour $STATUS_RIGHT_SEPARATOR_FG),bg=$(interpret_colour $STATUS_RIGHT_BG)]#[nobold]
	#[fg=$(interpret_colour $STATUS_RIGHT_NORMAL_FG),bg=$(interpret_colour $STATUS_RIGHT_BG)]"
status_right_time_style="#[fg=$(interpret_colour $STATUS_RIGHT_TIME_FG),bg=$(interpret_colour $STATUS_RIGHT_BG)]#[bold]"
status_right_sysstat_component="
	#($HOME/.tmux/bin/mem-cpu-load.sh)
	#{battery_icon}
	#[fg=$(interpret_colour $STATUS_RIGHT_NORMAL_FG),bg=$(interpret_colour $STATUS_RIGHT_BG)]
	#{?#{!=:#{battery_percentage},}
		,#{battery_percentage} #{battery_remain}
		,
	}"
if [ "$(uname -s)" != "Darwin" ]; then
	status_right_sysstat_part="${status_right_sysstat_component} ${status_right_sep_component} "
fi
status_right_state_hint_component="
	#{?client_prefix
		,#[fg=$(interpret_colour $STATUS_RIGHT_BG)#,bg=$(interpret_colour $STATUS_BAR_MAIN_BG)]
			#[fg=$(interpret_colour $STATUS_RIGHT_NORMAL_FG)#,bg=$(interpret_colour $STATUS_RIGHT_BG)]
			 󰘴 
			#[fg=$(interpret_colour $STATUS_RIGHT_BG)#,bg=$(interpret_colour $STATUS_BAR_MAIN_BG)] 
		,
	}
"
# ---
tmp="
	${status_right_state_hint_component}
	#[fg=$(interpret_colour $STATUS_RIGHT_BG),bg=$(interpret_colour $STATUS_BAR_MAIN_BG)]
	${status_right_normal_style} 
	${status_right_sysstat_part}
	#{host_short} 
	${status_right_sep_component} 
	%a %d %b
	${status_right_time_style} %H:%M "
tmux set -g status-right "$(echo "$tmp" | tr -d \\n\\t)"

tmux set -g status-right-length 100

# Window status
tmp=" 
	#{?#{!=:#{client_key_table},off}
		,#[bold]
		,
	}
	#I:#W#($HOME/.tmux/bin/render-window-flags.sh '#F#{?pane_synchronized,S,}')
	#[nobold] "
tmux set -g window-status-format "$(echo "$tmp" | tr -d \\n\\t)"

tmp="
	#{?#{!=:#{client_key_table},off}
		,#[fg=$(interpret_colour $STATUS_CURR_BG)#,bg=$(interpret_colour $STATUS_MAIN_BG)]
			#[fg=$(interpret_colour $STATUS_CURR_FG)#,bg=$(interpret_colour $STATUS_CURR_BG)]
		,
	}
	#[bold] #I:#W#(~/.tmux/bin/render-window-flags.sh '#F#{?pane_synchronized,S,}')
	#[nobold]
	#{?#{==:#{client_key_table},off}
		,[] 
		, #[fg=$(interpret_colour $STATUS_CURR_BG)#,bg=$(interpret_colour $STATUS_BAR_MAIN_BG)]
	}"
tmux set -g window-status-current-format "$(echo "$tmp" | tr -d \\n\\t)"

# Window with activity status
tmux set -g window-status-activity-style "reverse"

# Window with a bell alert
tmux set -g window-status-bell-style "reverse,blink"

# Window separator
tmux set -g window-status-separator ""

# Window status alignment (absolute-centre is too new, Mar 2021)
tmux set -g status-justify absolute-centre 2> /dev/null || tmux set -g status-justify centre

# Pane border
tmux set -g pane-border-style "bg=$(interpret_colour $PANE_BORDER_BG),fg=$(interpret_colour $PANE_BORDER_FG)"
tmux set -g pane-active-border-style "bg=$(interpret_colour $PANE_ACTIVE_BORDER_BG),fg=$(interpret_colour $PANE_ACTIVE_BORDER_FG)"

# Pane number indicator
tmux set -g display-panes-colour colour$PANE_NUM_INDICATOR_COLOUR
tmux set -g display-panes-active-colour colour$PANE_NUM_INDICATOR_ACTIVE_COLOUR

# Clock mode
tmux set -g clock-mode-colour colour$CLOCK_COLOUR
tmux set -g clock-mode-style 24

# Message
tmux set -g message-style "bg=$(interpret_colour $MESSAGE_BG),fg=$(interpret_colour $MESSAGE_FG)"

# Command message
tmux set -g message-command-style "bg=$(interpret_colour $MESSAGE_COMMAND_BG),fg=$(interpret_colour $MESSAGE_COMMAND_FG)"

# Mode
tmux set -g mode-style "bg=$(interpret_colour $MODE_BG),fg=$(interpret_colour $MODE_FG)"
