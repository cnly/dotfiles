#! /bin/bash

# Modifies PATH so the "tmux" command points to the correct binary in case there are multiple.
# TODO: Darwin is not supported (lack of /proc)
# TODO: Will this break if /tmp has noexec?

set -eo pipefail

[ "$(uname -s)" != "Darwin" ] || exit 0

tmux_server_pid=$1

# We try to get the path to the server bin in two ways
cmdline_bin=$(cat /proc/$tmux_server_pid/cmdline | strings -1 | head -n1) || true
if [[ "$cmdline_bin" =~ ^/.+ ]]; then
	tmux_bin=$cmdline_bin
else
	# The following will stop working if the binary gets deleted during upgrade, etc.
	tmux_bin=$(realpath /proc/$tmux_server_pid/exe)
fi

which_bin=$(which tmux || true)  # Can be empty if tmux not installed on system
which_bin=${which_bin:-$tmux_bin}
socket_dir=$(dirname $($tmux_bin display-message -p '#{socket_path}'))

# Already the desired one?
[ "$(realpath $which_bin)" != "$(realpath $tmux_bin)" -o \
	"$(dirname $(dirname $which_bin))" != "$socket_dir" ] || exit 0

tmpdir=$(mktemp -p "$socket_dir" -d -t "tmux-path.$tmux_server_pid.XXXXXXXXXX")

ln -sf $tmux_bin $tmpdir/tmux
$tmux_bin set-environment -g PATH $tmpdir:$PATH
