#! /bin/bash

# Adapted from https://github.com/Townk/tmux-config/blob/master/scripts/window-flags

FLAGS=$1

FLAGS=${FLAGS/\*/}  # *: Current window flag
FLAGS=${FLAGS/-/^}  # -: Last window flag

# FLAGS=${FLAGS/S/S}    # S: Synchronized (custom flag)
# FLAGS=${FLAGS/\#/ }  # #: Window activity flag
# FLAGS=${FLAGS/~/ }   # ~: Window silence flag
# FLAGS=${FLAGS/!/ }   # !: Window bell flag
# FLAGS=${FLAGS/Z/ }   # Z: Window zoomed flag
# FLAGS=${FLAGS/M/ }   # M: Window marked flag

[ -n "$FLAGS" -a "${FLAGS:0:1}" != "^" ] && FLAGS=":$FLAGS"

echo "${FLAGS}"
