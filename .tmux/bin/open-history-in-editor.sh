#! /usr/bin/env bash
set -x
file=$(mktemp)
tmux capture-pane -JpS - > "$file"

current_pane=$(tmux display-message -p '#{session_name}:#{window_index}.#{pane_index}')
current_workdir=$(tmux display-message -p '#{pane_current_path}')
session_name="tmp-$(echo "$file" | cut -d. -f2)"
target_pane="$session_name"

# shellcheck disable=SC2016
vim_selector='{ vim_cmd=~/.local/bin/nv && [ -x $vim_cmd ]; } ||
	{ vim_cmd=/home/linuxbrew/.linuxbrew/bin/nvim && [ -x $vim_cmd ]; } ||
	{ vim_cmd=/usr/local/bin/nvim && [ -x $vim_cmd ]; } ||
	{ vim_cmd=nvim && type -p nvim &> /dev/null; } ||
	vim_cmd=vim'

auto_fixed_trailing_spaces_msg='Automatically fixed trailing whitespaces.'
vim_opts=''
vim_opts+=" '+set ft=log'"
vim_opts+=" '+normal! G'"
vim_opts+=" '+silent! keeppatterns %s/\\v\\s+$//g'"
vim_opts+=" '+silent! w'"
vim_opts+=" '+echo \"$auto_fixed_trailing_spaces_msg\"'"
# vim_opts+=" '+call writefile([getpid()], \"$file.pid\")'"
vim_opts+=" $file"

pkill_cmd="pkill -f '$auto_fixed_trailing_spaces_msg.*$file'"

tmux new-session -d -s "$session_name" "$SHELL" -i -c " \
	$vim_selector; \
	cd '$current_workdir'; \
	\$vim_cmd $vim_opts; \
	[ \$(jobs | wc -l) = 0 ] || { echo '\nWarning: Automatically killing lingering processes'; $pkill_cmd; sleep 1; }; \
	rm $file; \
	tmux swap-pane -s $current_pane -t $target_pane; \
	"

tmux swap-pane -s "$current_pane" -t "$target_pane"
