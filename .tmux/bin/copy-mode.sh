#! /bin/sh

if ~/.tmux/bin/nested-session.sh; then
	tmux send-prefix \; send-keys Escape
else
	tmux copy-mode
fi
