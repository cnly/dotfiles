#! /bin/sh

[ -f /proc/meminfo ] || exit 0  # Not a linux

top_result=$(top -bn 2 -d 0.5)

echo \
	$(cat /proc/meminfo | grep -F MemAvailable: | awk '{printf "%.1fG", $2/1024/1024}') \
	$(echo "$top_result" | grep '^%Cpu' | tail -n 1 | awk '{printf "%.0f%%", $2+$4+$6}') \
	$(echo "$top_result" | grep '^top -' | tail -n 1 | awk '{print substr($(NF-2), 1, length($(NF-2))-1)}')
