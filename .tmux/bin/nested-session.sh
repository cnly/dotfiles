#! /bin/sh

window_panes=$(tmux display-message -p '#{window_panes}')
pane_current_command=$(tmux display-message -p '#{pane_current_command}')

[ "$window_panes" = "1" ] && [ \
	"$pane_current_command" = "mosh-client" -o \
	"$pane_current_command" = "ssh" \
	];
