#! /usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$SCRIPT_DIR"

rm -f -- *.json

for file in *.yml; do
	echo "Converting $file"
	yq . "$file" > "${file%.yml}.json"
done
