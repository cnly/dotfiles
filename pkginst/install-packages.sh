#! /bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
pushd "$SCRIPT_DIR"

source ../shlib/detect-env.sh
source ../shlib/utils.sh

set -euxo pipefail

export PATH=$HOME/.local/bin:$DEFAULT_BREW_BIN_DIR:$PATH

bb_exec=$(ensure_bb) || exit 1

if command_not_exists brew; then
	! is_in_docker || export CI=1  # Non-interactive brew install in docker
	./brew-install.sh
fi

user_cfg=./user.edn
if [ ! -f "$user_cfg" ]; then
	mixins=""
	mixins+="${dotfiles_lightweight_install:+ :lightweight}"
	echo "{:envspec $(platform_os_version), :mixins [$mixins]}" > "$user_cfg"
fi

set +x
echo -e "Current user config $user_cfg:\n---"
cat "$user_cfg"
echo "---"
bash_ask_yn_default_yes "Is this okay?" || exit 1

cmds=$("$bb_exec" ./dispatch.clj config.edn "$user_cfg")
echo "About to execute:"
echo "---"
echo "$cmds"
echo "---"
bash_ask_yn_default_yes "Is this okay?" || exit 1

set -x
eval "$cmds"

[ -z "${dotfiles_linuxbrew_clean_cache:-}" ] || brew cleanup --prune=all
