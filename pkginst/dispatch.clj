#! /usr/bin/env bb

(ns ^{:clj-kondo/config '{:linters
                          {:unresolved-symbol
                           {:exclude [(dispatch/err-if> [$])
                                      (dispatch/err-if-not> [$])
                                      (dispatch/err-if>> [$])
                                      (dispatch/err-if-not>> [$])]}}}}
  dispatch
  (:require [clojure.string :as s]
            [clojure.edn :as edn]
            [clojure.java.io :as io])
  (:import [java.io PushbackReader]))

(defmacro err-if>
  "In the test, use $ to refer to the value."
  [v test ex-msg]
  `(let [~'$ ~v]
     (if ~test
       (throw (RuntimeException. ~ex-msg))
       ~'$)))

(defmacro err-if>> [test ex-msg v]
  `(err-if> ~v ~test ~ex-msg))

(defmacro err-if-not> [v test ex-msg]
  `(err-if> ~v (not ~test) ~ex-msg))

(defmacro err-if-not>> [test ex-msg v]
  `(err-if-not> ~v ~test ~ex-msg))

(defn map-kv [kf vf coll]
  (reduce-kv #(assoc %1 (kf %2) (vf %3)) {} coll))

(defn map-k [kf coll]
  (map-kv kf identity coll))

(defn map-v [vf coll]
  (map-kv identity vf coll))

(defn maybe-expand-alias [aliases x]
  (if (keyword? x)
    (if (contains? aliases x)
      (x aliases)
      (throw (RuntimeException. (str "Unknown alias: " x))))
    x))

(defn normalise-pkg-def [aliases m1 m2 pkgname]
  (->> (merge {:package pkgname
               :install nil}
              m1 m2)
       (map-v (partial maybe-expand-alias aliases))))

(comment
  (normalise-pkg-def {:a1 "XX" :a2 "YY"} nil {:install :a1} "test"))

(defn pkgs-vec->map
  "Converts a vector packages list to a map of pkg definitions, expanding aliases.
  Note that after this fn, some packages may still have {:install nil} which needs
  to be replaced later with the default install option AND ALSO alias-expanded."
  [aliases v]
  (loop [key (first v) maybe-val (second v) more (nnext v)
         normalise (partial normalise-pkg-def aliases nil) ret {}]
    (if-not key
      ret
      (cond
        (map? key)
        (cond
          (-> key meta ::with)
          (recur maybe-val (first more) (next more)
                 (partial normalise-pkg-def aliases key) ret)
          :else (throw (RuntimeException. (str "Unknown map found in packages: " key))))

        (and (map? maybe-val) (not (meta maybe-val)))
        (recur (first more) (second more) (nnext more)
               normalise (merge ret {key (normalise maybe-val key)}))

        :else (recur maybe-val (first more) (next more)
                     normalise (merge ret {key (normalise nil key)}))))))

(comment
  (pkgs-vec->map {:a1 "XX"}
                 ["tmux"
                  "zsh"
                  "vim" {:package "vimmm" :install :a1}
                  ^::with {:install ["brew" "install"]}
                  "vim-nox"
                  "neovim"]))

(defn profile-includes-vec->map
  "Converts the :includes vector in p to map, expanding aliases found in p."
  [{:keys [aliases includes] :as p}]
  (cond-> p
    (vector? includes)
    (update :includes (partial pkgs-vec->map aliases))))

(comment
  (profile-includes-vec->map {:aliases {:a1 "XX"}
                              :includes ['a 'b {:install :a1} 'c]}))

;; https://stackoverflow.com/questions/17327733/merge-two-complex-data-structures
(def deep-merge
  "Like merge, but if the value corresponding to the conflicting key is a map,
  don't replace the whole map altogether."
  (partial merge-with (fn [x y] (cond (map? y) (deep-merge x y)
                                      :else y))))

(defn profiles-apply-excludes
  "Removes entries from p's :includes using entries from q's :excludes"
  [p q]
  (-> p
      (dissoc :excludes)  ;; Note that we can't exclude packages for self
      (update :includes #(apply dissoc % (:excludes q)))))

(defn merge-profiles
  [profiles]
  (->> profiles
       (map profile-includes-vec->map)
       (reduce #(-> %1
                    (profiles-apply-excludes %2)
                    (deep-merge %2))
               nil)))

(comment
  (merge-profiles [{:aliases {:a "XX"}
                    :defaults {:install "brew install"}
                    :includes ["b" {:install :a} "c"]
                    :excludes ["d"]}
                   {:aliases {:a "YY"}
                    :defaults {:install "apt install"}
                    :includes ["a" {:install :a}
                               "z" {:install "asdf install"}]
                    :excludes ["c"]}
                   {:includes [^::with {:install "brew install --cask"}
                               "d" "e"]}]))

(defn profile->cmds
  "Reduces the given normalised profile (can be obtained using merge-profiles)
  to a sequence of commands or a script."
  ([p]
   (profile->cmds true p))
  ([to-script p]
   (when p
     (let [{:keys [aliases]} p  ;; Aliases at root are correct as profiles have been merged
           default-install (->> p :defaults :install (maybe-expand-alias aliases))
           fill-pdef-install (fn [{i :install, :as pd}]
                               (let [i (or i default-install)]
                                 (assoc pd :install (if (string? i) [i] i))))
           cmds (->> (:includes p)
                     vals  ;; Discard the keys (user-friendly names)
                     (map fill-pdef-install)
                     (group-by :install)  ;; Make a map from install commands to pdefs
                     (map-v #(map :package %))  ;; Extract package names in pdefs
                     (map-kv #(filter some? %) #(filter some? %))  ;; Remove nils
                     (map-kv #(map str %) #(map str %))
                     (map-v sort)  ;; Make it more deterministic
                     (map #(concat (key %) (val %)))  ;; Compose commands
                     ;; Sort the commands to make it more deterministic
                     (sort-by first))]
       (if-not to-script
         cmds
         (->> cmds
              (map (partial s/join " "))
              (s/join "\n")))))))

(comment
  (profile->cmds
   false
   {:defaults {:install ["apt" "install"]}
    :includes {1 {:package 1, :install nil}
               'a {:package 'a, :install ["dnf" nil "install"]}
               2 {:package nil :install "./install-2.sh"}
               3 {:package 3, :install ["brew" "install" "--cask"]}
               4 {:package 4, :install ["brew" "install"]}
               5 {:package 5, :install nil}}}))

(defn expand-profile-path
  "The given profile path must have been normalised (looks like [:a :b] or []).
  Returns a sequence of new paths.
  Usually you want mapcat."
  [p]
  (when p  ;; If p is nil, return nil
    (if-not (seq p)
      [[]]  ;; If p is the empty path, no need to expand
      (->> p
           (iterate butlast)
           (take-while some?)
           reverse
           (cons [])  ;; Add the root profile ("empty path")
           (map vec)
           (into [])))))

#_
(expand-profile-path [:linux :debian :11])
;; => [[] [:linux] [:linux :debian] [:linux :debian :11]]

(defn calc-aliases-at-path
  "Returns the merged aliases map at the given path in profiles.
  path is an expanded profile path, shaped like ks for get-in."
  [profiles path]
  (->> path
       expand-profile-path
       (map #(conj % :aliases))
       (map (partial get-in profiles))
       (reduce merge)))

(comment
  (calc-aliases-at-path {:aliases {:a "XX"}
                         :linux {:aliases {:a "YY" :b "ZZ"}}}
                        [:linux]))

(defn active-profiles->script
  "active-paths contains zero or more key sequences, each like the ks argument for get-in."
  [profiles active-paths]
  (let [active-profiles (map (partial get-in profiles) active-paths)
        ap-alias-maps (map (partial calc-aliases-at-path profiles) active-paths)
        ;; We assoc the correct alias maps into the profiles for later expansion
        active-profiles (map #(assoc %1 :aliases %2) active-profiles ap-alias-maps)]
    (->> active-profiles
         merge-profiles
         profile->cmds)))

(comment
  (active-profiles->script {:aliases {:a ["XX"]}
                            :default {:includes ["tmux"
                                                 "zsh"
                                                 "neovim"]}
                            :linux {:default {:defaults {:install "apt install"}}
                                    :ubuntu {:18.04 {:excludes ["neovim"]
                                                     :includes ["neovim" {:install :a
                                                                          :package "nnn"}
                                                                "this" {:install "brew install"}]}}}
                            :darwin {:defaults {:install :brew}
                                     :includes ["alt-tab"]}}
                           [[:default] [:linux :default] [:nonexist] [:linux :ubuntu :18.04]]
                           ;; ()
                           ))

(defn normalise-profile-path [p]
  (when p
    (if (sequential? p)
      p
      (-> p name (s/split #":") (->> (mapv keyword))))))

(comment
  (normalise-profile-path :p:a:t:h))

(def normalise-expand-path
  "Usually you want mapcat."
  (comp expand-profile-path normalise-profile-path))

(defn envspec->profile-paths [matchers envspec]
  (->> matchers
       (map-k re-pattern)
       (filter #(re-find (key %) (cond (keyword? envspec) (name envspec)
                                       :else (str envspec))))
       (sort-by (comp - count str key))  ;; Longer regexes first
       vals
       first
       ;; Note that a matcher can only specify one profile
       ;; But it can be expanded to multiple paths
       normalise-expand-path))

(comment
  (envspec->profile-paths {"^Darwin.+" :darwin
                           "^linux-debian.+" :linux:debian:unstable
                           "^linux-debian-10$" :linux:debian:old
                           "^test$" [:linux :debian :sid]},
                          "linux-debian-11"
                          ;; "test"
                          ;; "Darwin-"
                          ))

(defn config->script [{:keys [profiles matchers envspec mixins]}]
  (->> envspec
       (envspec->profile-paths matchers)
       (#(concat % (map normalise-profile-path mixins)))
       (active-profiles->script profiles)))

(defn cfg-profile-exist?
  "Path must be normalised."
  [config path]
  (when path
    (get-in config (cons :profiles path))))

(defn validate-merged-config [{:keys [matchers envspec mixins] :as config}]
  (let [e #(throw (ex-info % {:type :config-validation-error}))
        unknown-keys (-> config
                         (dissoc :profiles :matchers :envspec :mixins)
                         keys)
        envspec-profile (->> envspec
                             (envspec->profile-paths matchers)
                             last)  ;; The ones before the last are from expansion
        unknown-mixin-profiles (->> mixins
                                    (map normalise-profile-path)
                                    (filter #(not (cfg-profile-exist? config %))))]
    (when (seq unknown-keys)
      (e (str "Unknown config keys: " (s/join ", " unknown-keys))))
    (when (coll? envspec) (e "Envspec cannot be a coll"))
    (when-not (every? #(or (coll? %) (keyword? %)) mixins)
      (e "Mixins should contain path specs, each like [:a :b] or :a:b"))
    (when-not (or (some? envspec) (seq mixins))
      (e "No profile will be selected"))
    (when-not (cfg-profile-exist? config envspec-profile)
      (e (str "Envspec and matchers gave an unknown profile path: " (pr-str envspec-profile))))
    (when (seq unknown-mixin-profiles)
      (e (str "Non-existing profile path(s) selected by mixins: "
              (s/join ", " unknown-mixin-profiles))))))

(comment
  (validate-merged-config {:unknown-key1 [], :unknown-key2 ""})
  (validate-merged-config {:envspec []})
  (validate-merged-config {:mixins ["a"]})
  (validate-merged-config {:envspec nil, :mixins nil})
  (validate-merged-config {:envspec "A"})
  (validate-merged-config {:matchers {"A" :a} :envspec "A"})
  (validate-merged-config {:profiles {:a {}}, :matchers {"A" :a}, :envspec "A", :mixins [:a :b]}))

(defn merge-configs
  "Merges multiple configs into one.
  NOTE: Results of profile path conflicts are undefined; don't let that happen :)"
  [configs]
  (let [merge-cfgs-maps (fn [k] (->> configs (map k) (reduce deep-merge)))
        merge-envspecs (fn [k] (->> configs (map k) (filter some?) last))
        merge-mixins (fn [k] (some->> configs (map k) (filter some?) seq (apply concat)))]
    (assoc {}
           :profiles (merge-cfgs-maps :profiles)
           :matchers (merge-cfgs-maps :matchers)
           :envspec (merge-envspecs :envspec)
           :mixins (merge-mixins :mixins))))

(def ^:private map-start-char "{")  ;; This { can mess up my editor :(
(defn user-input->merged-config
  "Takes a sequence of user input strings and generate a merged config."
  [inputs]
  (let [read-with #(with-meta % {::with true})]
    (->> inputs
         (filter seq)
         (map #(cond (s/starts-with? % map-start-char) (edn/read-string %)
                     :else (edn/read {:readers {'with read-with}}
                                     (PushbackReader. (io/reader %)))))
         merge-configs)))

(comment
  (-> ["config.edn"
       ;; (pr-str {:matchers {"wowsoempty" []}})
       ;; (pr-str {:envspec 'linux-debian-11, :mixins []})
       (pr-str {:envspec 'darwin-12, :mixins []})
       (pr-str {:mixins []})]
      user-input->merged-config
      config->script))

(defn perr [& args]
  (binding [*out* *err*]
    (apply println args)))

(defn perr-exit [code & args]
  (apply perr args)
  (System/exit code))

(defn -main [& _args]
  (if (> (count *command-line-args*) 0)
    (let [{:keys [matchers envspec mixins] :as mcfg} (user-input->merged-config *command-line-args*)]
      (perr (count *command-line-args*) "configs merged.")
      (perr "\tEnvspec:" envspec
            "=> Paths:" (envspec->profile-paths matchers envspec))
      (perr "\tMixins:" mixins
            "=> Paths:" (mapv normalise-profile-path mixins))
      (when (and (some? mixins) (empty? mixins))
        (perr "Note: Mixins is empty coll. If you want to specify the root profile, use :mixins [[]]"))
      (try
        (validate-merged-config mcfg)
        (catch RuntimeException e
          (if (= :config-validation-error (-> e ex-data :type))
            (perr-exit 2 (str "Validation error: " (ex-message e)))
            (throw (RuntimeException. e)))))
      (perr "---")
      (println (config->script mcfg)))
    (perr-exit 1 "Usage: Pass one or more strings or file paths as configs.")))

(defn handler
  "Event handler for the vercel-babashka runtime."
  [event]
  (let [params (or (:form-params event) (:params event))
        {:keys [cfg user-cfg]} params
        mcfg (user-input->merged-config [cfg user-cfg])
        {:keys [matchers envspec mixins]} mcfg]
    (or (try
          (validate-merged-config mcfg)
          (catch RuntimeException e
            (if (= :config-validation-error (-> e ex-data :type))
              {:status 400
               :body (str "Validation error: " (ex-message e))}
              (throw e))))
        (str "# Envspec: " envspec " => Paths: " (envspec->profile-paths matchers envspec) "\n"
             "# Mixins: " (pr-str mixins) " => Paths: " (pr-str (mapv normalise-profile-path mixins)) "\n\n"
             (config->script mcfg)))))

(when (= (System/getProperty "babashka.file") *file*)
  (-main))
