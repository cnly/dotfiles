#! /bin/bash

# This script generates Ahead-of-Time-Dispatched (AoTD) package installation commands
# for faster docker image building.

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
pushd "$SCRIPT_DIR"

source ../shlib/utils.sh

set -euxo pipefail

outfile=$1
envspec=$2
mixins=":docker-build ${3:-}"

bb_exec=$(ensure_bb "$(mktemp -d)")
"$bb_exec" --version

aotd_cmds=$("$bb_exec" ./dispatch.clj config.edn "{:envspec $envspec, :mixins [$mixins]}")

cat <<-EOF > "$outfile"
#! /bin/bash
set -euxo pipefail
export PATH=\$HOME/.local/bin:/home/linuxbrew/.linuxbrew/bin:\$PATH
export HOMEBREW_NO_AUTO_UPDATE=1
$aotd_cmds
brew cleanup --prune=all
EOF
chmod +x "$outfile"

set +x
echo "AoTD commands:"
cat "$outfile"
