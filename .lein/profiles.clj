{:user {:shadow-cljs {:dependencies [[refactor-nrepl "2.5.0"]
                                     [cider/cider-nrepl "0.24.0"]
                                     [iced-nrepl "1.0.1"]]}
        :dependencies [[com.bhauman/rebel-readline "0.1.4"]
                       [hashp "0.2.0"]]
        :injections [(require 'hashp.core)]
        :aliases {"rebl" ["trampoline" "run" "-m" "rebel-readline.main"]}
        :plugins [[cider/cider-nrepl "0.24.0"]
                  [lein-ancient "0.6.15"]]}}
