ARG baseimage=debian:testing
FROM $baseimage

# https://serverfault.com/a/1016972
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

# When building with buildkit, homebrew may fail to recognise the docker
# environment so manually add this file
RUN touch /.dockerenv

# Gzipped indexes slow down apt search too much
RUN rm /etc/apt/apt.conf.d/docker-gzip-indexes

# Set the working directory.
WORKDIR /root

# We install some common packages so they can be cached
# curl: For homebrew
# vim: We use it instead of nvim to run PlugUpdate without a terminal, taking
# advantage of (read: abusing) https://github.com/junegunn/vim-plug/issues/499.
# Also we install it to prevent update-alternatives from being too smart with (n)vim
RUN apt update && apt install -y \
	command-not-found \
	curl \
	git \
	man-db \
	python3 \
	python3-pip \
	python3-venv \
	sed \
	silversearcher-ag \
	sudo \
	unzip \
	vim \
	vim-nox \
	zip \
	zsh

# Without setting this tmux may not display fancy characters properly
ENV LC_ALL=C.UTF-8

COPY ./etc/sudoers.d/preserve-proxy-env-vars /etc/sudoers.d/
RUN chmod 440 /etc/sudoers.d/preserve-proxy-env-vars

CMD ["/bin/zsh"]

# To update the command-not-found database
RUN apt update

COPY ./pkginst/brew-install.sh ./dotfiles/pkginst/
RUN CI=1 ./dotfiles/pkginst/brew-install.sh

RUN HOMEBREW_NO_AUTO_UPDATE=1 \
	/home/linuxbrew/.linuxbrew/bin/brew install \
	borkdude/brew/babashka \
	neovim \
	node \
	&& \
	/home/linuxbrew/.linuxbrew/bin/brew cleanup --prune=all

COPY ./pkginst/docker-aotd-cmds-* ./pkginst/*-install.sh ./dotfiles/pkginst/
RUN cd ./dotfiles/pkginst && ./docker-aotd-cmds-light
ARG lightweight
RUN [ -n "$lightweight" ] || ( cd ./dotfiles/pkginst && ./docker-aotd-cmds-full )

COPY ./.minivimrc ./.vimrc ./coc-settings.json ./install.sh ./update.sh ./dotfiles/
COPY ./nvim/ ./dotfiles/nvim/
COPY ./shlib/ ./dotfiles/shlib/
RUN export dotfiles_linuxbrew_clean_cache=1 \
	dotfiles_linuxbrew_no_update=1 \
	dotfiles_go_clean_cache=1 \
	dotfiles_pip_clean_cache=1; \
	./dotfiles/install.sh link_vim_nvim_files && \
	./dotfiles/update.sh \
		update_pip_modules \
		update_vim_plugins \
		update_nvim_treesitter \
		update_coc_nvim
RUN [ -n "$lightweight" ] || \
	dotfiles_linuxbrew_clean_cache=1 \
	dotfiles_linuxbrew_no_update=1 \
	dotfiles_go_clean_cache=1 \
	dotfiles_pip_clean_cache=1 \
	./dotfiles/update.sh update_vim_go update_gdb_tools

COPY . ./dotfiles
RUN dotfiles_lightweight_install=$lightweight \
	dotfiles_linuxbrew_clean_cache=1 \
	dotfiles_linuxbrew_no_update=1 \
	dotfiles_go_clean_cache=1 \
	dotfiles_pip_clean_cache=1 \
	dotfiles_install_skip=install_packages \
	dotfiles_update_skip=update_pip_modules,update_vim_plugins,update_nvim_treesitter,update_coc_nvim,update_vim_go,update_gdb_tools \
	./dotfiles/install.sh

RUN ./.oh-my-zsh/custom/themes/powerlevel10k/gitstatus/install
