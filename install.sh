#! /bin/bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "$BASEDIR/shlib/detect-env.sh"
source "$BASEDIR/shlib/utils.sh"

if command_not_exists $realpath_exe; then
	echo 'It seems that GNU realpath is not installed.'
	echo 'On macOS, try: brew install coreutils'
	exit 1
fi

set -x

avail_funcs() {
	"$grep_exe" -E '^function (.+)\(\).+' "${BASH_SOURCE[0]}" | "$sed_exe" -E 's|^function (.+)\(\).*|\1|'
}

ensure_link() {
	{ local -; set +x; } 2> /dev/null

	local link_src=$1
	local link_target=$2

	mkdir -p $(dirname $link_target)
	local link_src=$($realpath_exe --relative-to=$(dirname $link_target) $link_src)

	if [ -e "${link_target}" -o -h "${link_target}" ]; then
		echo "Found ${link_target}. Checking if it's what we want..."
		if [ ! -h "${link_target}" ]; then
			echo "Not a link. Renaming to ${link_target}-pre-dotfiles and proceeding..."
			mv "${link_target}" "${link_target}"-pre-dotfiles
		else
			if [ "$(readlink "${link_target}")" = "${link_src}" ]; then
				echo 'Yes. No action needed.'
				return
			else
				echo "Does not link to our target. Renaming to ${link_target}-pre-dotfiles and proceeding..."
				mv "${link_target}" "${link_target}"-pre-dotfiles
			fi
		fi
	fi

	echo "Linking ${link_src} to ${link_target}..."
	mkdir -p "$(dirname "${link_target}")"
	ln -s "${link_src}" "${link_target}"
}

function install_packages() {
	bash "${BASEDIR}"/pkginst/install-packages.sh || exit 1
}

function install_oh_my_zsh() {
	# Part of this function comes from https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh
	if [ -z "$ZSH" ]; then
		ZSH=~/.oh-my-zsh
	fi
	if [ -d "$ZSH" ]; then
		echo "You already have Oh My Zsh installed."
		echo "We'll still ensure it's what we want."
		echo "And if you want to reinstall Oh My Zsh, remove $ZSH."
	else
		umask g-w,o-w
		env git clone --depth=1 https://github.com/ohmyzsh/ohmyzsh.git "$ZSH" || {
			echo "Error: git clone of oh-my-zsh repo failed"
			exit 1
		}
	fi

	ensure_link "${BASEDIR}/.zshrc" ~/.zshrc

	# If this user's login shell is not already "zsh", attempt to switch.
	TEST_CURRENT_SHELL=$(basename "$SHELL")
	if [ "$TEST_CURRENT_SHELL" != "zsh" ]; then
		zsh_path=$(grep /zsh$ /etc/shells | tail -1)
		if [ "$(passwd -S | cut -d' ' -f2)" = "P" -o "$UID" = "0" ]; then
			# If we have a password (which chsh wants) or we're root, do chsh
			echo "chshing to zsh..."
			chsh -s $zsh_path
		elif command -v sudo &> /dev/null; then
			echo "sudo-chshing to zsh..."
			sudo chsh -s $zsh_path $USER
		else
			echo "not trying to chsh automatically due to unmet conditions"
		fi
	fi

	return
}

function link_tmux_files() {
	ensure_link "${BASEDIR}/.tmux" ~/.tmux
	ensure_link "${BASEDIR}/.tmux.conf" ~/.tmux.conf
}

function link_ag_files() {
	ensure_link "${BASEDIR}/.agignore" ~/.agignore
}

function link_dir_colors_files() {
	ensure_link "${BASEDIR}/.dir_colors" ~/.dir_colors
}

function link_lein_files() {
	local f="/.lein/profiles.clj"
	ensure_link "${BASEDIR}$f" "$HOME$f"
}

function link_shadow_cljs_files() {
	local f="/.shadow-cljs/config.edn"
	ensure_link "${BASEDIR}$f" "$HOME$f"
}

function link_gdb_files() {
	ensure_link "${BASEDIR}/gdb/.gdbinit" ~/.gdbinit
	mkdir -p "${BASEDIR}/installed/gdb-tools"
	ensure_link "${BASEDIR}/installed/gdb-tools" ~/gdb-tools
	for x in peda pwndbg gef; do
		ensure_link "${BASEDIR}/gdb/gdb-$x" "${BASEDIR}/installed/bin/gdb-$x"
	done
}

function link_proxychains_files() {
	ensure_link "${BASEDIR}/.proxychains" ~/.proxychains
}

function link_vim_nvim_files() {
	ensure_link "${BASEDIR}/.vimrc" ~/.vimrc
	ensure_link "${BASEDIR}/coc-settings.json" ~/.vim/coc-settings.json
	ensure_link "${BASEDIR}/.vimrc" ~/.config/nvim/init.vim
	ensure_link "${BASEDIR}/coc-settings.json" ~/.config/nvim/coc-settings.json
}

function link_direnv_files() {
	ensure_link "${BASEDIR}/direnv" ~/.config/direnv
}

function link_linuxbrew_bin() {
	if [ -d /home/linuxbrew/.linuxbrew/bin ]; then
		ln -s /home/linuxbrew/.linuxbrew/bin ~/bbin
	fi
}

check_proxy() {
	[ -z "${PROXYCHAINS_CONF_FILE}" ] || return 0
	if [ -z "${http_proxy}" ] || ! git config --global -l 2> /dev/null | grep -q proxy; then
		bash_ask_yn_default_yes "It seems you haven't set http_proxy or git proxy. Continue?" || exit 0
	fi
}

invoke_fn() {
	set -x
	$1
	set +x
}

main() {
	check_proxy

	# shellcheck disable=SC2046  # We want resplitting
	"$0" $(avail_funcs) || exit 1
	bash "${BASEDIR}"/update.sh || exit 1
}

mkdir -p ~/.local/bin

export HOMEBREW_NO_AUTO_UPDATE=$dotfiles_linuxbrew_no_update

set +x
if [ "$#" != 0 ]; then
	if [ "$1" = 'help' ]; then
		set +x
		echo -e '\nAvailable functions:'
		avail_funcs | sed 's/^/    /'
		exit 0
	fi

	IFS=', ' read -r -a skips <<< "${dotfiles_install_skip:-}"
	for func in "$@"; do
		func=$(echo "$func" | tr '-' '_')
		if ! typeset -f "${func}" > /dev/null; then
			if typeset -f "link_${func}" > /dev/null; then
				func="link_${func}"
			else
				func="install_${func}"
			fi
		fi
		# shellcheck disable=SC2076
		if [[ " ${skips[*]} " =~ " ${func} " ]]; then
			echo "Skipping function ${func}..."
			continue
		fi
		echo "Running function ${func}..."
		invoke_fn "$func"
	done
else
	main
	echo 'Installation completed.'
fi

# vim: set fdm=marker:
