#! /bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
pushd "$SCRIPT_DIR"

set -euxo pipefail

variant=$1
baseimage_version=$2  # E.g. 20211201
output_image_basename=$3  # E.g. myimage

case "$variant" in
	*latest)
		( cd pkginst && ./gen-docker-aotd-cmds.sh docker-aotd-cmds-light linux-debian- ":lightweight" )
		( cd pkginst && ./gen-docker-aotd-cmds.sh docker-aotd-cmds-full linux-debian- )
		;;
	*bullseye)
		( cd pkginst && ./gen-docker-aotd-cmds.sh docker-aotd-cmds-light linux-debian-11 ":lightweight" )
		( cd pkginst && ./gen-docker-aotd-cmds.sh docker-aotd-cmds-full linux-debian-11 )
		;;
	*)
		echo "Unknown variant $variant."
		exit 1
		;;
esac

case "$variant" in
	latest)
		DOCKER_BUILDKIT=1 \
			docker build --pull -t "$output_image_basename" \
			--build-arg BUILDKIT_INLINE_CACHE=1 \
			--build-arg baseimage=debian:unstable-"$baseimage_version" \
			--build-arg lightweight=1 \
			--cache-from "$output_image_basename:latest" \
			.
		;;
	full-latest)
		DOCKER_BUILDKIT=1 \
			docker build --pull -t "$output_image_basename-full" \
			--build-arg BUILDKIT_INLINE_CACHE=1 \
			--build-arg baseimage=debian:unstable-"$baseimage_version" \
			--cache-from "$output_image_basename:latest,$output_image_basename-full:latest" \
			.
		;;
	bullseye)
		DOCKER_BUILDKIT=1 \
			docker build --pull -t "$output_image_basename:bullseye" \
			--build-arg BUILDKIT_INLINE_CACHE=1 \
			--build-arg baseimage=debian:bullseye-"$baseimage_version" \
			--build-arg lightweight=1 \
			--cache-from "$output_image_basename:bullseye" \
			.
		;;
	full-bullseye)
		DOCKER_BUILDKIT=1 \
			docker build --pull -t "$output_image_basename-full:bullseye" \
			--build-arg BUILDKIT_INLINE_CACHE=1 \
			--build-arg baseimage=debian:bullseye-"$baseimage_version" \
			--cache-from "$output_image_basename:bullseye,$output_image_basename-full:bullseye" \
			.
		;;
	*)
		echo "Unknown variant $variant."
		exit 1
		;;
esac
