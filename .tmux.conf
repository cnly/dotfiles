if '[ "$(uname -s)" != "Darwin" ]' \
	'set -g default-terminal "tmux-256color"' \
	'set -g default-terminal "xterm-256color"'

# set -ga terminal-overrides ",*256col*:Tc"  # Enable true colour support

# We use this to ensure programs invoked by tmux config files execute the correct tmux binary.
# TODO: Darwin isn't supported yet.
run '~/.tmux/bin/setup-tmux-path.sh #{pid}'

%if #{>=:#{version},3.3}
# https://github.com/tmux/tmux/issues/3218
set -g allow-passthrough on
%endif

set-option -g prefix C-f
bind -T prefix C-f send-prefix
bind -T root C-b send-prefix
bind -T prefix C-b 'send-keys C-b'

set-option -g mode-keys vi
set-option -g wrap-search off
set-option -g status-position top
set-option -g history-limit 50000
set-option -g mouse on
set-option -g renumber-windows on
set-option -g base-index 1
set -g status-interval 5
set -sg escape-time 1

# https://github.com/yetamrra/termcopy
set -s set-clipboard on
set -sa terminal-overrides ',xterm*:XT:Ms=\E]52;%p1%s;%p2%s\007'
set -sa terminal-overrides ',tmux*:Ms=\\E]52;%p1%s;%p2%s\\007'
set -sa terminal-overrides ',screen*:Ms=\\E]52;%p1%s;%p2%s\\007'

# Improve mouse support: https://github.com/tmux/tmux/issues/140#issuecomment-474341833 {{{

# disable "release mouse drag to copy and exit copy-mode", ref: https://github.com/tmux/tmux/issues/140
unbind-key -T copy-mode-vi MouseDragEnd1Pane

# since MouseDragEnd1Pane neither exit copy-mode nor clear selection now,
# let single click do selection clearing for us.
bind-key -T copy-mode-vi MouseDown1Pane select-pane\; send-keys -X clear-selection

# this line changes the default binding of MouseDrag1Pane, the only difference
# is that we use `copy-mode -eM` instead of `copy-mode -M`, so that WheelDownPane
# can trigger copy-mode to exit when copy-mode is entered by MouseDrag1Pane
bind -n MouseDrag1Pane if -Ft= '#{mouse_any_flag}' 'if -Ft= "#{pane_in_mode}" "copy-mode -eM" "send-keys -M"' 'copy-mode -eM'

# }}}

unbind-key -T prefix Space
unbind-key -T prefix n
unbind-key -T prefix p

bind-key -T prefix c new-window -a
%if #{>=:#{version},3.2}
bind-key -T prefix C new-window -b
%endif
bind-key -T prefix i command-prompt -p '(monitor silence)' 'set-window-option monitor-silence %%'

bind-key -T copy-mode Escape send-keys -X cancel
bind-key -T copy-mode-vi Escape send-keys -X cancel

bind-key Escape run-shell "~/.tmux/bin/copy-mode.sh"  # Also works for C-Escape and C-[
bind-key [ run-shell "~/.tmux/bin/copy-mode.sh"

# Note: Prefix [ can be used for local copy-mode

bind-key u \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send-keys u' \
		'copy-mode; send-keys -X halfpage-up'

bind-key C-u \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send-keys C-u' \
		'copy-mode; send-keys -X halfpage-up'

# C-S-/
bind-key BSpace \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send BSpace' \
		'command-prompt -p "(search up)" "copy-mode; send -X search-backward \"%%%\""'

bind-key ? \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send ?' \
		'command-prompt -p "(search up)" "copy-mode; send -X search-backward \"%%%\""'

# C-/
bind-key ^_ \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send ^_' \
		'command-prompt -p "(search down)" "copy-mode; send -X search-forward \"%%%\""'

bind-key / \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send /' \
		'command-prompt -p "(search down)" "copy-mode; send -X search-forward \"%%%\""'

bind-key -T prefix h list-keys

bind -n M-C-h \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-h' 'select-pane -L'
bind -n M-C-j \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-j' 'select-pane -D'
bind -n M-C-k \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-k' 'select-pane -U'
bind -n M-C-l \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-l' 'select-pane -R'
bind-key -n M-C-m next-window
bind-key -n M-Enter next-window
bind-key -n M-C-n previous-window

bind \{ \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys \{' 'swap-pane -U'
bind \} \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys \}' 'swap-pane -D'

bind-key z \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys z' 'resize-pane -Z'
bind-key C-z \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys C-z' 'resize-pane -Z'
bind-key -T root M-C-z \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-z' 'last-pane; resize-pane -Z'

unbind-key -T prefix 9
bind-key -T prefix 9 select-window -t '{end}'

bind-key -n M-C-e \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-e' 'select-layout even-horizontal'
bind-key -n M-C-c \
	if '[ "#{window_panes}" = "1" ]' 'send-keys M-C-c' 'select-layout main-horizontal'
bind -r Space \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys Space' 'next-layout'

# Search for the prompt
%if #{>=:#{version},3.4}
bind -n M-C-p \
	if "~/.tmux/bin/nested-session.sh" \
		'send M-C-p' \
		'copy-mode; send-keys C-p'
bind -T copy-mode-vi C-p 'send-keys -X previous-prompt'
bind -T copy-mode-vi C-n 'send-keys -X next-prompt'
%else
bind -n M-C-p \
	if "~/.tmux/bin/nested-session.sh" \
		'send M-C-p' \
		'copy-mode; send-keys -X start-of-line; send-keys -X search-backward "(╰|╰)"'
%endif

bind k confirm-before -p "respawn-pane #P? (y/n)" "respawn-pane -k"
bind C-s \
	if '[ "#{window_panes}" = "1" ]' 'send-prefix; send-keys C-s' 'set-window-option synchronize-panes'
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'
bind -T copy-mode-vi u send-keys -X halfpage-up
bind -T copy-mode-vi d send-keys -X halfpage-down
bind ` set-option -g mouse \; refresh-client -S

bind C-e \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send-keys C-e' \
		"run-shell ~/.tmux/bin/open-history-in-editor.sh"

bind \; \
	if "~/.tmux/bin/nested-session.sh" \
		'send-prefix; send-keys ";"' \
		"last-pane"

bind-key C-l \
	if -F '#{?#{==:#{session_windows},1},only-one-window,}' \
		'send-prefix; send-keys "l"' \
		last-window
bind l \
	if -F '#{?#{==:#{session_windows},1},only-one-window,}' \
		'send-prefix; send-keys "l"' \
		last-window

bind "\\" split-window -h -c "#{pane_current_path}"
bind "C-\\" split-window -h -c "#{pane_current_path}"
bind "'" split-window -c "#{pane_current_path}"
bind "C-'" split-window -c "#{pane_current_path}"

bind -n M-C-Space \
	set prefix None \; \
	set key-table off \; \
	if -F "#{pane_in_mode}" "send-keys -X cancel" \; \
	refresh-client -S \; \

bind -T off M-C-Space \
	set -u prefix \; \
	set -u key-table \; \
	refresh-client -S \; \

run-shell ~/.tmux/apply-theme.sh

set -g @plugin 'Cnly/tmux-battery'
# Icons from FontAwesome (4); see .zshrc
set -g @batt_charged_icon " #[fg=green]#[none]"
set -g @batt_charging_icon " #[fg=colour208]#[none]"
set -g @batt_attached_icon " #[fg=colour208]󰂑#[none]"
set -g @batt_full_charge_icon "   "
set -g @batt_high_charge_icon "   "
set -g @batt_medium_charge_icon "    #[none]"
set -g @batt_low_charge_icon " #[fg=colour208]   #[none]"
set -g @batt_very_low_charge_icon " #[fg=colour196]   #[none]"
set -g @batt_remain_short true
set -g @batt_remaining_update_interval 15

set -g @plugin 'nhdaly/tmux-better-mouse-mode'
set -g @emulate-scroll-for-no-mouse-alternate-buffer 'on'

# vim: set ft=tmux fdm=marker:

# To install TPM: git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
