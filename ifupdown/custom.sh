#! /usr/bin/env bash

# https://manpages.debian.org/testing/ifupdown/interfaces.5.en.html

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

ip_exec=/sbin/ip
iptables_exec=/sbin/iptables
ip6tables_exec=/sbin/ip6tables

mode_is() {
	local wanted=$1
	[ "$wanted" = "$MODE" ]
}

phase_is() {
	local wanted=$1
	[ "$wanted" = "$PHASE" ]
}

# This does two things:
# 1. Properly split the line into an array ${varname}_arr, expanding variables
# and stripping comments inside
# 2. (Re)create the ${varname} variable using the sanitised array
# Note that this can't create local variables.
process_line() {
	local __line_content=$1
	local __varname=${2:-line}
	# Use newlines to prevent inline comments from eating the ')'
	eval "${__varname}_arr=(
	$__line_content
	)"
	# Use @Q to quote each element in array
	eval "$__varname=\${${__varname}_arr[@]@Q}"
}

# Using execute expands the variables inside the command string and echos it.
# Treat this function like sh -c. If the arguments in the command string passed
# in contain spaces, they must be properly quoted, which can be done with
# process_line().
execute() {
	local cmd=$1
	[ "$VERBOSITY" = 0 ] || echo "[#] $cmd"
	eval "$cmd"
}

has_colon() {
	local line=$1
	[[ "$line" == *:* ]]
}

has_dev_arg() {
	local line_arr=("$@")
	for arg in "${line_arr[@]}"; do
		if [ "$arg" = 'dev' ]; then
			return 0
		fi
	done
	return 1
}

infer_ip_ver() {
	local line=$1
	local force_ipver=$2
	if [ -n "$force_ipver" ]; then
		ipver=$force_ipver
	else
		has_colon "$line" && ipver=6 || ipver=4
	fi
	echo $ipver
}

handle_env() {
	execute "$IF_ENV"
}

handle_ip_link() {
	process_line "$IF_IP_LINK"
	if phase_is pre-up; then
		execute "$ip_exec link add $IFACE $line"
	elif phase_is post-down; then
		execute "$ip_exec link del $IFACE || true"
	fi
}

# Converts -A CHAIN or -I CHAIN [num] to -D CHAIN.
# Overwrites the original variable with the given name.
# Returns whether a corresponding deletion command is generated.
iptables_cmd_to_del() {
	local line_name=$1
	process_line "${!line_name}" line
	for i in "${!line_arr[@]}"; do
		local arg=${line_arr[$i]}
		if [ "$arg" == "-A" ] || [ "$arg" == "-I" ]; then
			line_arr[$i]="-D"
			if [ "$arg" == "-I" ] && [[ "${line_arr[$((i+2))]}" =~ ^[0-9]+$ ]]; then
				unset "line_arr[$((i+2))]"
			fi
			should_execute=1
			break
		fi
	done
	[ -n "$should_execute" ] || return 1
	# Unsetting elements can create gaps so recreate the array
	line_arr=("${line_arr[@]}")
	eval "$line_name=\${line_arr[@]@Q}"
}

handle_iptables() {
	local use_ipv6=$1

	phase_is pre-up || phase_is post-down || return 0

	if [ -n "$use_ipv6" ]; then
		local curr_iptables_exec=$ip6tables_exec
		local conf_lines="$IF_IP6TABLES"
	else
		local curr_iptables_exec=$iptables_exec
		local conf_lines="$IF_IPTABLES"
	fi

	while IFS= read -r line; do
		process_line "$line"

		if phase_is pre-up; then
			execute "$curr_iptables_exec $line"
		else
			iptables_cmd_to_del line || continue
			execute "$curr_iptables_exec $line || true"
		fi

	done <<< "$conf_lines"
}

handle_iptables_chain() {
	local use_ipv6=$1

	phase_is pre-up || phase_is post-down || return 0

	if [ -n "$use_ipv6" ]; then
		local curr_iptables_exec=$ip6tables_exec
		local conf_lines="$IF_IP6TABLES_CHAIN"
	else
		local curr_iptables_exec=$iptables_exec
		local conf_lines="$IF_IPTABLES_CHAIN"
	fi

	while IFS= read -r line; do
		process_line "$line"
		local chain_name=${line_arr[0]@Q}
		unset 'line_arr[0]'
		line_arr=("${line_arr[@]}")
		line="${line_arr[@]@Q}"

		if phase_is pre-up; then
			execute "$curr_iptables_exec -N $chain_name"
			execute "$curr_iptables_exec $line -j $chain_name"
		else
			if iptables_cmd_to_del line; then
				execute "$curr_iptables_exec $line -j $chain_name || true"
			fi
			execute "$curr_iptables_exec -F $chain_name || true"
			execute "$curr_iptables_exec -X $chain_name || true"
		fi

	done <<< "$conf_lines"
}

exec_ip_cmd() {
	local ipver=$1  # 4 or 6
	local subcmd=$2  # E.g. link, rule, route
	local paramstr=$3

	if mode_is start; then
		local action=add
	else
		local action=del
		local or_true='|| true'
	fi

	execute "$ip_exec -${ipver} $subcmd $action $paramstr $or_true"
}

handle_ip_rule() {
	local force_ipver=$1
	declare -n conf_lines="IF_IP${force_ipver}_RULE"
	local ipver=

	phase_is pre-up || phase_is post-down || return 0

	while IFS= read -r line; do
		process_line "$line"
		if [ -n "$force_ipver" ]; then
			ipver=$force_ipver
		else
			has_colon "$line" && ipver=6 || ipver=4
		fi
		exec_ip_cmd $ipver rule "$line"
	done <<< "$conf_lines"
}

handle_ip_addr_route() {
	local subcmd=$1  # addr or route
	local force_ipver=$2
	declare -n conf_lines="IF_IP${force_ipver}_${subcmd^^}"

	phase_is post-up || phase_is pre-down || return 0

	while IFS= read -r line; do
		process_line "$line"
		local ipver=$(infer_ip_ver "$line" $force_ipver)
		local dev_flag=
		has_dev_arg "${line_arr[@]}" || dev_flag="dev $IFACE"
		exec_ip_cmd $ipver $subcmd "$line $dev_flag"
	done <<< "$conf_lines"
}

handle_multicast_snooping() {
	phase_is post-up || return 0
	process_line "$IF_MULTICAST_SNOOPING"
	execute "echo $line > /sys/class/net/$IFACE/bridge/multicast_snooping"
}

hook() {
	[ -z "$IF_ENV" ] || handle_env

	[ -z "$IF_IP_LINK" ] || handle_ip_link

	if mode_is start; then
		[ -z "$IF_IPTABLES_CHAIN" ] || handle_iptables_chain
		[ -z "$IF_IP6TABLES_CHAIN" ] || handle_iptables_chain 1
	fi

	[ -z "$IF_IPTABLES" ] || handle_iptables
	[ -z "$IF_IP6TABLES" ] || handle_iptables 1

	if mode_is stop; then
		[ -z "$IF_IPTABLES_CHAIN" ] || handle_iptables_chain
		[ -z "$IF_IP6TABLES_CHAIN" ] || handle_iptables_chain 1
	fi

	[ -z "$IF_IP_ADDR" ] || handle_ip_addr_route addr  # Auto detect version
	[ -z "$IF_IP4_ADDR" ] || handle_ip_addr_route addr 4
	[ -z "$IF_IP6_ADDR" ] || handle_ip_addr_route addr 6

	[ -z "$IF_IP_RULE" ] || handle_ip_rule  # Auto detect version
	[ -z "$IF_IP4_RULE" ] || handle_ip_rule 4
	[ -z "$IF_IP6_RULE" ] || handle_ip_rule 6

	[ -z "$IF_IP_ROUTE" ] || handle_ip_addr_route route  # Auto detect version
	[ -z "$IF_IP4_ROUTE" ] || handle_ip_addr_route route 4
	[ -z "$IF_IP6_ROUTE" ] || handle_ip_addr_route route 6

	[ -z "$IF_MULTICAST_SNOOPING" ] || handle_multicast_snooping
}

if [ "$#" = 1 ] && [ "$1" = '--install' ]; then
	set -x
	script_name=custom
	! [ -e "/etc/network/${script_name}.sh" ] || exit 1
	cp "${BASH_SOURCE[0]}" "/etc/network/${script_name}.sh"
	chmod +x "/etc/network/${script_name}.sh"
	for phase in down post-down pre-up up; do
		ln -s "../${script_name}.sh" "/etc/network/if-${phase}.d/${script_name}"
	done
	exit
fi

if [ -n "$ADDRFAM" ] && [ "$ADDRFAM" != 'meta' ]; then
	hook
	exit
fi
