#! /bin/bash

set -e

user=$USER
release=testing
mirror=$(cat /etc/apt/sources.list | grep '^deb' | head -n1 | cut -d\  -f2)
timezone=$(cat /etc/timezone)
ssh_key=$(ssh-add -L | head -n1)

# https://stackoverflow.com/questions/34095839/cloud-init-what-is-the-execution-order-of-cloud-config-directives

tmpfile=$(mktemp)
cat > $tmpfile <<EOF
#cloud-config
# vim: set ft=yaml:

write_files:
  - path: /etc/sysctl.d/smaller-pid-max.conf
    content: |
      kernel.pid_max = 99999
  - path: /etc/sysctl.d/oom-kill-allocating-task.conf
    content: |
      vm.oom_kill_allocating_task = 1
  - path: /etc/sudoers.d/preserve-proxy-env-vars
    content: |
      Defaults env_keep += "http_proxy HTTP_PROXY"
      Defaults env_keep += "https_proxy HTTPS_PROXY"
      Defaults env_keep += "ftp_proxy FTP_PROXY"
      Defaults env_keep += "RSYNC_PROXY"
      Defaults env_keep += "no_proxy NO_PROXY"
      Defaults env_keep += "all_proxy ALL_PROXY"

users:
  - name: $user
    shell: /bin/bash
    lock_passwd: true
    groups: sudo, adm, docker
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - $ssh_key

apt:
  primary:
    - arches:
        - default
      uri: $mirror
  security:
    - arches:
        - default
      uri: $mirror
# Not sure why release will be n/a if not specified manually
  sources_list: |
    deb \$PRIMARY $release main contrib non-free
    # deb-src \$PRIMARY $release main contrib non-free
    # deb \$SECURITY $release-security main contrib non-free

packages:
  - curl
  - dnsutils
  - git
  - htop
  - mtr-tiny
  - proxychains-ng
  - vim

timezone: $timezone

runcmd:
  - sysctl --system
EOF

# cp $tmpfile $user-cloud-config.yaml

cloud-localds $user-seed.img $tmpfile

rm $tmpfile
