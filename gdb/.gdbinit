define init-peda
source ~/gdb-tools/peda/peda.py
end
document init-peda
Initializes the PEDA (Python Exploit Development Assistant for GDB) framework
end

define init-pwndbg
source ~/gdb-tools/pwndbg/gdbinit.py
set syntax-highlight-style autumn
set show-flags on
set auto-save-search on
end
document init-pwndbg
Initializes PwnDBG
end

define init-gef
source ~/gdb-tools/gef/gef.py
end
document init-gef
Initializes GEF (GDB Enhanced Features)
end

# vim: set ft=gdb:
