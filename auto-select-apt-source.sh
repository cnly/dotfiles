#! /bin/bash

set -ex

if [ "$UID" != 0 ]; then
	echo "Please run me as root!"
	exit 1
fi

source /etc/os-release
# TODO: The security repository?
case $ID in
	debian)
		official_source=deb.debian.org
		;;
	ubuntu)
		official_source=archive.ubuntu.com
		;;
	*)
		echo "I can't handle anything other than debian and ubuntu :("
		exit 1
esac

command -v curl || (apt update && apt install -y curl)

original_source=$(sed -En "s|^deb .+://(.+)/$ID.+|\1|p" /etc/apt/sources.list | head -n1)
if [ -z "$original_source" ]; then
	echo "Cannot determine original source"
	exit 1
fi

possible_sources=(
	opentuna.cn
	mirrors.cloud.tencent.com
	$official_source
	$original_source
	)

best_source=$(printf "%s\n" "${possible_sources[@]}" | \
	xargs -n1 -I^ -t \
	sh -c "echo \`curl -m 5 -r 0-102400 -s -w %{speed_download} -o /dev/null ^/$ID/ls-lR.gz\` ^" \
	| sort -g -r | head -1 | awk '{print $2}'
)

if [ -z "$best_source" ]; then
	echo "No best source found"
	exit 1
fi

if [ "$best_source" != "$original_source" ]; then
	sed -i.bak "s|$original_source|$best_source|g" /etc/apt/sources.list
fi
