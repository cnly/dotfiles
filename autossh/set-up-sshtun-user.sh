#! /bin/bash

set -e

if [ "$UID" != 0 ]; then
	echo "Please run me as root!"
	exit 1
fi

tun_user=${1:-sshtunuser}
echo "Using username: $tun_user"

if id $tun_user &> /dev/null; then
	echo "Error: user already exists"
	echo "To delete, run:"
	echo "	deluser $tun_user"
	exit 1
fi

set -x

useradd -s /bin/true -m $tun_user

mkdir -p /home/$tun_user/.ssh
touch /home/$tun_user/.ssh/authorized_keys

chown -R $tun_user:$tun_user /home/$tun_user/.ssh
chmod 700 /home/$tun_user/.ssh
chmod 600 /home/$tun_user/.ssh/authorized_keys

if [ -d /etc/ssh/sshd_config.d ]; then
	target_config_file=/etc/ssh/sshd_config.d/$tun_user.conf
	[ ! -e "$target_config_file" ] || mv "$target_config_file" "$target_config_file".bak
else
	target_config_file=/etc/ssh/sshd_config
	cp "$target_config_file" "$target_config_file".bak
fi

cat >> "$target_config_file" << EOF
Match User $tun_user
	GatewayPorts yes
	ForceCommand /bin/false
	ClientAliveInterval 30
	ClientAliveCountMax 3
EOF

systemctl reload ssh

set +x

echo "Done! Use the following command to add authorised SSH keys to the user:"
echo "cat | sudo tee -a /home/$tun_user/.ssh/authorized_keys"
