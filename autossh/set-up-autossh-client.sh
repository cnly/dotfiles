#! /bin/bash

set -e

if [ "$#" -eq 1 ]; then
	host=$1
	tun_user=sshtunuser
	port=22
	foward_remote_port=10022
elif [ "$#" -eq 4 ]; then
	tun_user=$1
	port=$2
	host=$3
	foward_remote_port=$4
else
	echo "Usage: $0 host; OR: $0 tun_user port host foward_remote_port"
	exit 1
fi

if [ ! -f ~/.ssh/autossh_$tun_user ]; then
	ssh-keygen -b 4096 -t rsa -f ~/.ssh/autossh_$tun_user -q -N ""
	echo "Generated new key pair. Public key:"
	cat ~/.ssh/autossh_$tun_user.pub
fi

echo "Testing connection..."
ssh -o PasswordAuthentication=no \
	-o ExitOnForwardFailure=yes \
	-R $foward_remote_port:localhost:22 \
	-i ~/.ssh/autossh_$tun_user -p $port $tun_user@$host || {
	echo "Connection failed :("
	exit 1
}

dpkg -l autossh &> /dev/null || {
	sudo apt install -y autossh || apt install -y autossh
}

mkdir -p ~/.config/systemd/user

cat << EOF > ~/.config/systemd/user/autossh.service
[Unit]
Description=Autossh
Wants=network-online.target
After=network-online.target
StartLimitIntervalSec=0

[Service]
ExecStart=/usr/bin/autossh -M 0 -N \\
	-o "ServerAliveInterval 30" \\
	-o "ServerAliveCountMax 3" \\
	-o "ConnectTimeout 10" \\
	-o "ExitOnForwardFailure yes" \\
	-o "CheckHostIP no" \\
	-i /home/$USER/.ssh/autossh_$tun_user -p $port $tun_user@$host \\
	-R $foward_remote_port:localhost:22
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
EOF

systemctl --user daemon-reload
systemctl --user edit --full autossh.service
systemctl --user enable --now autossh

echo 'You might also want to run the following command:'
echo "loginctl enable-linger $USER || sudo loginctl enable-linger $USER"
