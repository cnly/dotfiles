#! /usr/bin/env zsh
# zmodload zsh/zprof

dotfiles_dir=$(dirname $(realpath ~/.zshrc))
source "$dotfiles_dir/shlib/detect-env.sh"
source "$dotfiles_dir/shlib/utils.sh"

is_in_docker && export TERM=xterm-256color

# Reclaim our ctrl-s/ctrl-q hotkeys
stty -ixon

# Fix zsh Home/End keys problems with TERM=xterm-256color
bindkey "\033[1~" beginning-of-line
bindkey "\033[4~" end-of-line

for x in \
	{,/usr}/sbin /usr/local/{,s}bin \
	$DEFAULT_BREW_PREFIX/{,s}bin \
	$HOME/.local/bin \
	; do
	add_to_path $x prepend
done
for x in \
	/usr/local/go/bin $HOME/{go,.cargo,.npm-packages}/bin \
	$HOME/.vim/bundle/{fzf,vim-iced}/bin \
	"$dotfiles_dir"/installed{,/venv}/bin "$dotfiles_dir"/shlib \
	; do
	add_to_path $x
done

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

export HOMEBREW_NO_AUTO_UPDATE=1

# Load theme on startup
[ -f ~/.theme ] || echo "light" > ~/.theme
[ "$(cat ~/.theme 2> /dev/null)" = "dark" ] && THEME="dark" || THEME="light"

ZSH_AUTOSUGGEST_USE_ASYNC=1
ZSH_AUTOSUGGEST_MANUAL_REBIND=1 # May cause problems: https://github.com/zsh-users/zsh-autosuggestions#disabling-automatic-widget-re-binding

# For zlong_alert.zsh
__zlong_default_duration=60
zlong_duration=$__zlong_default_duration
zlong_ignore_cmds='vim ssh nvim n nv st mt mosh'
zlong_use_notify_send=false

fzf_vim_bundle_location_pattern="$HOME/.vim/*"
fzf_brew_location_pattern="$DEFAULT_BREW_BIN_DIR/*"
if [[ "$(command -v fzf)" == ${~fzf_vim_bundle_location_pattern} ]]; then
	export FZF_BASE=$HOME/.vim/bundle/fzf
elif [[ "$(command -v fzf)" == ${~fzf_brew_location_pattern} ]]; then
	# Since the fzf plugin runs `brew --prefix` which is too slow
	export FZF_BASE=/home/linuxbrew/.linuxbrew/opt/fzf
fi
# Include hidden files by default
# export FZF_DEFAULT_COMMAND='find . -path "*/\.git/*" -prune -o -print |
	# sed s/^..// 2> /dev/null'
FZF_BINDINGS="--bind="
FZF_BINDINGS=$FZF_BINDINGS"alt-p:toggle-preview,"
FZF_BINDINGS=$FZF_BINDINGS"shift-down:preview-page-down,"
FZF_BINDINGS=$FZF_BINDINGS"shift-up:preview-page-up"
export FZF_DEFAULT_OPTS="$FZF_BINDINGS \
	--history=${HOME}/.fzf-history \
	--color=$THEME"

# For fzf zsh plugin
# https://github.com/junegunn/fzf/wiki/Configuring-shell-key-bindings
FZF_COMPLETION_TRIGGER=',,'
# They hide hidden files/dirs by default. Change that.
FZF_ALT_C_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.git/*' \\) -prune -o -type d -print 2> /dev/null | cut -b3-"
FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200' --height 100%"
FZF_CTRL_T_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.git/*' \\) -prune \
	-o -print 2> /dev/null | cut -b3-"
FZF_CTRL_T_OPTS="--preview 'batcat -f {} 2> /dev/null || bat -f {} 2> /dev/null || tree -C {} 2> /dev/null' --height 100%"
FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:99%:wrap:hidden --height 100%"

# Manual for powerlevel*k:
# https://github.com/Powerlevel9k/powerlevel9k
# https://github.com/romkatv/powerlevel10k

POWERLEVEL9K_MODE="nerdfont-complete"

POWERLEVEL9K_TIME_ICON=''

POWERLEVEL9K_STATUS_OK_ICON='\uf42e'

POWERLEVEL9K_VCS_BRANCH_ICON=

POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=1

# https://github.com/romkatv/powerlevel10k/issues/730
# POWERLEVEL9K_VIRTUALENV_SHOW_WITH_PYENV=false
# POWERLEVEL9K_VIRTUALENV_SHOW_WITH_PYENV=if-different
POWERLEVEL9K_VIRTUALENV_SHOW_PYTHON_VERSION=false
POWERLEVEL9K_VIRTUALENV_VISUAL_IDENTIFIER_EXPANSION=''
POWERLEVEL9K_PYENV_VISUAL_IDENTIFIER_EXPANSION=''

function _pyenv_prompt_style_hook() {
	if [[ -n "$VIRTUAL_ENV" ]]; then
		POWERLEVEL9K_PYENV_CONTENT_EXPANSION='+'
	else
		unset POWERLEVEL9K_PYENV_CONTENT_EXPANSION
	fi
}

typeset -ag precmd_functions
if [[ -z ${precmd_functions[(r)_pyenv_prompt_style_hook]} ]]; then
  precmd_functions=( ${precmd_functions[@]} _pyenv_prompt_style_hook )
fi
typeset -ag chpwd_functions
if [[ -z ${chpwd_functions[(r)_pyenv_prompt_style_hook]} ]]; then
  chpwd_functions=( ${chpwd_functions[@]} _pyenv_prompt_style_hook )
fi

if [ "$USER" = "$DEFAULT_USER" ]; then
	POWERLEVEL9K_CONTEXT_TEMPLATE=
elif is_in_docker; then
	POWERLEVEL9K_CONTEXT_TEMPLATE='%n'  # Don't show hostname (%m)
fi
if [ "$(hostname)" = "ubuntu" -o "$(hostname)" = "debian" ]; then
	ip_suffix=$(hostname -I | cut -d' ' -f 1 | cut -d. -f 3-4)
	[ -n "$ip_suffix" ] && POWERLEVEL9K_CONTEXT_TEMPLATE="%n@.$ip_suffix"
fi

ZLE_RPROMPT_INDENT=0
POWERLEVEL9K_TERM_SHELL_INTEGRATION=true
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_LEGACY_ICON_SPACING=true
# POWERLEVEL9K_ICON_PADDING=none
# POWERLEVEL9K_LEFT_LEFT_WHITESPACE=
# POWERLEVEL9K_RIGHT_RIGHT_WHITESPACE=
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_LEFT_PROMPT_LAST_SEGMENT_END_SYMBOL=''
POWERLEVEL9K_RIGHT_PROMPT_FIRST_SEGMENT_START_SYMBOL=''
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_CHAR='·'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon_joined context_joined time_joined virtualenv_joined pyenv_joined anaconda_joined dir_joined vcs_joined)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(history background_jobs command_execution_time status)

# About the %F{...} things below: They are expanded by ZSH. See:
# http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html

function apply_theme() {
	THEME=$(cat ~/.theme 2> /dev/null)
	[ "$THEME" = 'dark' ] || THEME=light  # Default to light

	if [ "$THEME" = "dark" ]; then

		PROMPT_FOREGROUND="251"
		PROMPT_THEME_ACCENT=${PROMPT_THEME_ACCENT_DARK:-039}

		POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_FOREGROUND="240"
		POWERLEVEL9K_STATUS_ERROR_BACKGROUND="196"
		unset POWERLEVEL9K_STATUS_ERROR_FOREGROUND
		POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND="231"

		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=8"

		export BAT_THEME="Monokai Extended"

	else

		PROMPT_FOREGROUND="239"
		PROMPT_THEME_ACCENT=${PROMPT_THEME_ACCENT_LIGHT:-027}

		POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_FOREGROUND="250"
		POWERLEVEL9K_STATUS_ERROR_BACKGROUND="218"
		POWERLEVEL9K_STATUS_ERROR_FOREGROUND="088"
		POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND="232"

		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=248"  # https://github.com/zsh-users/zsh-autosuggestions/issues/698

		export BAT_THEME="GitHub"

	fi

	# Common styles

	PROMPT_BACKGROUND="none"

	# PROMPT_BACKGROUND + PROMPT_THEME_ACCENT
	for x in OS_ICON \
		CONTEXT_DEFAULT CONTEXT_ROOT CONTEXT_SUDO CONTEXT_REMOTE CONTEXT_REMOTE_SUDO \
		VIRTUALENV PYENV ANACONDA \
		TIME; do
		eval "POWERLEVEL9K_${x}_BACKGROUND=$PROMPT_BACKGROUND"
		eval "POWERLEVEL9K_${x}_FOREGROUND=$PROMPT_THEME_ACCENT"
	done

	# PROMPT_BACKGROUND + PROMPT_FOREGROUND
	for x in DIR_HOME DIR_HOME_SUBFOLDER DIR_ETC DIR_DEFAULT \
		VCS_CLEAN VCS_UNTRACKED VCS_MODIFIED \
		HISTORY; do
		eval "POWERLEVEL9K_${x}_BACKGROUND=$PROMPT_BACKGROUND"
		eval "POWERLEVEL9K_${x}_FOREGROUND=$PROMPT_FOREGROUND"
	done

	# Override some settings above

	POWERLEVEL9K_VCS_CLEAN_FOREGROUND="green"
	POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND="001"
	POWERLEVEL9K_VCS_MODIFIED_FOREGROUND="003"

	POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND="none"

	POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND="$PROMPT_BACKGROUND"

	POWERLEVEL9K_STATUS_OK_BACKGROUND="$PROMPT_BACKGROUND"

	POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%{%F{$PROMPT_THEME_ACCENT}%K{$PROMPT_BACKGROUND}%}"
	is_in_docker && POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX+=" "
	POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%{%F{$PROMPT_THEME_ACCENT}%K{$PROMPT_BACKGROUND}%} ╰%{%b%f%k%F{$PROMPT_BACKGROUND}%} %{%f%}"

	export FZF_DEFAULT_OPTS=${FZF_DEFAULT_OPTS/--color=light/--color=$THEME}
	export FZF_DEFAULT_OPTS=${FZF_DEFAULT_OPTS/--color=dark/--color=$THEME}
	# For plugin fzf-tab: https://github.com/Aloxaf/fzf-tab/wiki/Configuration
	zstyle ':fzf-tab:*' fzf-flags "--color=$THEME" "${THEME/light/--no-bold}"
	zstyle ':fzf-tab:*' default-color ''
}

# Use source instead of exec zsh to prevent background jobs and unexported variables from disappearing
trap "source ~/.zshrc" USR1
trap "apply_theme" USR2

function apply_gnome_terminal_theme() {
	THEME=$1
	local profile_key_var_name="GNOME_TERMINAL_PROFILE_KEY_${THEME}"
	xdotool key --clearmodifiers shift+F10 r "${(P)profile_key_var_name}"
	# Can't use bash style indirection for zsh: https://misterpinchy.wordpress.com/2012/10/16/indirect-variable-references-in-bash-and-zsh/
}

# https://askubuntu.com/questions/416572/switch-profile-in-konsole-from-command-line
function apply_konsole_theme() {
	THEME=$1
	local color_scheme_var_name="KONSOLE_COLOR_SCHEME_${THEME}"
	local color_scheme=${(P)color_scheme_var_name}
	if [ -z "$color_scheme" ]; then
		[ "$THEME" = "dark" ] && color_scheme=WhiteOnBlack || color_scheme=BlackOnWhite
	fi

	if [ "$TMUX" ]; then
		printf '\033Ptmux;\033\033]50;colors=%s\007\033\\' "$color_scheme"
	else
		konsoleprofile "colors=$color_scheme"
	fi
}

function switch_theme() {
	if [ -n "$1" ]; then
		THEME=$1
	else
		THEME="$(cat ~/.theme 2> /dev/null)"
		if [ "$THEME" = "light" ]; then
			THEME="dark"
		else
			THEME="light"
		fi
	fi

	echo "$THEME" > ~/.theme

	[ "$TMUX" ] && tmux source-file ~/.tmux.conf

	which xdotool &> /dev/null && xdotool getactivewindow getwindowpid &> /dev/null && {
		is_gnome_terminal=$(ps --no-headers -o cmd --pid $(xdotool getactivewindow getwindowpid) | grep -q gnome-terminal && echo 1)
		if [ "$is_gnome_terminal" ]; then
			apply_gnome_terminal_theme $THEME
		elif is_darwin; then
			apply_konsole_theme $THEME
		fi
	} || {
		is_darwin || apply_konsole_theme $THEME
	}

	apply_theme
	pkill -USR2 -U $UID -f -- '-zsh'  # Compatible with BSD & Linux
}

test -r ~/.dir_colors && eval $(dircolors -b ~/.dir_colors)

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# https://superuser.com/questions/613685/how-stop-zsh-from-eating-space-before-pipe-symbol
ZLE_REMOVE_SUFFIX_CHARS=$' \t\n;'

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	direnv
	fzf
	git
	systemd
	extract
	command-not-found
	zsh-autosuggestions
	zsh-syntax-highlighting
	zlong_alert
)
if command_exists docker; then
	plugins+=docker
fi
if command_exists docker-compose; then
	plugins+=docker-compose
fi
if command_exists fzf; then
	plugins+=fzf-tab  # Needs to be the last to bind ^I
fi

if is_in_docker; then
	PROMPT_THEME_ACCENT_LIGHT=032
	PROMPT_THEME_ACCENT_DARK=032
fi

if [ -f ~/.theme_accent_colour ]; then
	colour=$(cat ~/.theme_accent_colour)
	PROMPT_THEME_ACCENT_DARK=$colour
	PROMPT_THEME_ACCENT_LIGHT=$colour
fi

if [ -f ~/.lzshrc ]; then
	source ~/.lzshrc
elif [ -f ~/.zsh_private ]; then
	source ~/.zsh_private
fi

apply_theme

FPATH="${DEFAULT_BREW_PREFIX}/share/zsh/site-functions:${FPATH}"

source $ZSH/oh-my-zsh.sh

# This speeds up pasting w/ autosuggest and syntax highlighting
# https://github.com/zsh-users/zsh-autosuggestions/issues/238
pasteinit() {
	OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
	zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
	zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish

# User configuration

setopt share_history
setopt append_history

# Complete hidden files, but not . and ..
setopt globdots
zstyle -e ':completion:*' special-dirs false

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

export LESS='-iMR'
export SYSTEMD_LESS='-iFRSXMK'
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Preferred editor
for x in nv nvim vim vi; do
	if command_exists $x; then  # command_exists ignores aliases
		export EDITOR=$(which $x)
		break
	fi
done

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

function __rclone_serve_http() {
	local protocol=$1 read_only=$2

	shift 2 || return 1
	local port=${1:-8080}

	shift &> /dev/null
	local baseurl_rand_n=${1:-0}
	local baseurl=/$(rand_num $baseurl_rand_n)

	set -x
	rclone serve $protocol $read_only . -L --addr '[::]:'$port --config /dev/null -v --baseurl $baseurl
}

# Example: rclone_serve_http 8080 6
function rclone_serve_http() {
	__rclone_serve_http http --read-only $@
}

function rclone_serve_webdav() {
	__rclone_serve_webdav webdav '' $@
}

function __rclone_serve_https() {
	local protocol=$1 read_only=$2 cert=$3 key=$4

	shift 4 || return 1
	local port=${1:-8443}

	shift &> /dev/null
	local baseurl_rand_n=${1:-0}
	local baseurl=/$(rand_num $baseurl_rand_n)

	set -x
	rclone serve $protocol $read_only . -L --cert "$cert" --key "$key" --addr '[::]:'$port --config /dev/null -v --baseurl $baseurl
}

# Example: rclone_serve_https /path/to/fullchain.cer /path/to/key 8443 6
function rclone_serve_https() {
	__rclone_serve_https http --read-only $@
}

function rclone_serve_webdavs() {
	__rclone_serve_https webdav '' $@
}
function bell() {
	if [ "$#" = 0 ]; then
		printf '\a'
		return
	fi

	local pref=$1
	case $pref in
		on)
			echo 'Bell will now always ring.'
			zlong_duration=0
			;;
		default)
			echo "Bell rings after $__zlong_default_duration seconds."
			zlong_duration=$__zlong_default_duration
			;;
		off)
			echo 'Bell will never ring.'
			zlong_duration=9223372036854775807
			;;
		[0-9]*)
			echo "Bell rings after $pref seconds."
			zlong_duration=$pref
			;;
		*)
			echo 'Usage: bell [on|off|default|<interval>]'
	esac
}

function ghpr() {
	local remote=$1
	local pr_id=$2
	local branch_name=$3
	[ -z "$branch_name" ] || branch_name="-$branch_name"
	local actual_branch_name=pr-$pr_id$branch_name
	set -x
	git fetch $remote pull/$pr_id/head:$actual_branch_name || return
	git checkout $actual_branch_name
}

function ds() {
	if [[ -n $1 ]]; then
		dirs "$@"
	else
		dirs -v | head -n 10
	fi
}

if [ -z "$SLACK_NOTIFY_WEBHOOK_URL" ] || command_not_exists jq; then
	alias slack-notify=__missing_webhook_url_or_jq
else
	alias slack-notify="SLACK_NOTIFY_WEBHOOK_URL=$SLACK_NOTIFY_WEBHOOK_URL slack-notify"
fi

alias a-c='apt-cache'
alias a-cm='apt-cache madison'
alias a-cp='apt-cache policy'
alias a-f='apt-file'
alias a-fs='apt-file search'
alias a='apt'
alias ag="ag --color-path='0;32' --color-line-number='0;33'"
alias ai='apt install'
alias aiy='apt install -y'
alias av=alias_value
alias b='brew'
alias bi='brew install'
alias br='bridge'
alias d='dvc'
alias dig6='dig aaaa'
alias dotfiles-update="$dotfiles_dir/update.sh"
alias endlessly='while true; do '
alias fzfp='fzf --preview="bat {} --color=always"'
alias gdb-multiarch-gef='gdb-multiarch -ex init-gef'
alias gdb-multiarch-peda='gdb-multiarch -ex init-peda'
alias gdb-multiarch-pwndbg='gdb-multiarch -ex init-pwndbg'
alias gdl='gd HEAD@{1}'
alias gloda='glod --all'
alias glodsa='glods --all'
alias i4='ip -4'
alias i6='ip -6'
alias ip='ip --color=auto'
alias lcmd=' fc -Lln -1'
alias ll='ls -la'
alias ls='ls --color=auto'  # Don't use -G, it has different meanings on mac and linux
alias m='man '
alias n='nv'
alias notify=slack-notify
alias np=unset_proxy
alias ns='n -S Session.vim'
alias nsl='nslookup'
alias pe='pyenv'
alias pp=print_proxy_settings
alias pro='proxychains4 '
alias proq='proxychains4 -q '
alias psppa='ps -wweo pid,ppid,args'
alias rand_Alnum="cat /dev/urandom | $tr_exe -dc a-zA-Z0-9 | head -c"
alias rand_alnum="cat /dev/urandom | $tr_exe -dc a-z0-9 | head -c"
alias rand_num="cat /dev/urandom | $tr_exe -dc 0-9 | head -c"
alias rclone-http='rclone --http-url'
alias rclone-webdav='rclone --webdav-url'
alias reload-zshes="pkill -USR1 -U $UID -f -- '-zsh'"
alias rp=realpath
alias s-a='ssh-add'
alias s1s=sha1sum
alias s='sudo'
alias sai='sudo apt install'
alias saiy='sudo apt install -y'
alias sc='systemctl'
alias screl='sc-reload'
alias scres='sc-restart'
alias scst='sc-status'
alias se='sudoedit'
alias sshloop='sshloop '
alias sl=sshloop
alias sudo='sudo '
alias ta='tmuxaon'
alias tar-lz4='tar --use-compress-program=lz4'
alias up=set_proxy
alias watch='watch '
alias zpo='zpool'

__macos_tailscale_exec=/Applications/Tailscale.app/Contents/MacOS/Tailscale
if is_darwin && [ -x "$__macos_tailscale_exec" ]; then
	alias ts="$__macos_tailscale_exec"
else
	alias ts="tailscale"
fi
alias tsip='ts ip -4'

command_exists bat || alias bat='batcat'

function generate_docker_run_vw_aliases() {
	local base=$1
	local vol_arg='"`pwd`":"`pwd`"'
	local workdir_arg='"`pwd`"'
	alias ${base}v="${base} -v $vol_arg"
	alias ${base}vw="${base} -v $vol_arg -w $workdir_arg"
}

# Don't combine short options for docker commands: https://github.com/docker/cli/issues/993
alias d-c='docker-compose'
alias dk='docker'
alias dki='docker image'
alias dkils='docker image ls'
alias dkirm='docker image rm'
alias dkr='docker run'
alias dkrd='dkr -d'
generate_docker_run_vw_aliases dkrd
alias dkrdrm='dkr -d --rm'
generate_docker_run_vw_aliases dkrdrm
alias dkrit='dkr -i -t'
generate_docker_run_vw_aliases dkrit
alias dkritrm='dkrit --rm'
generate_docker_run_vw_aliases dkritrm
alias dkritd='dkrit -d'
generate_docker_run_vw_aliases dkritd
alias dkritdrm='dkritd --rm'
generate_docker_run_vw_aliases dkritdrm
alias dke='docker exec'
alias dkeit='dke -i -t'
alias dkrm='docker rm'
alias dks='docker start'
alias dksai='docker start -a -i'
alias dkps='docker ps'
alias dkpsa='docker ps -a'
alias dkpl='docker pull'
alias dkl='docker logs'

function pdrun() {
	local defaults_file=$1
	local input_file=$2
	local output_fmt=$3
	local should_open=$4

	local output_file="${input_file%.*}.$output_fmt"

	[ $# = 3 -o $# = 4 ] || \
		(abort "Usage: $0 <defaults_file> <in_file> <out_format> [should_open]") || return

	(set -x && pandoc -d "$defaults_file" -o "$output_file" "$input_file") && {
		set +x
		[ -z "$should_open" ] && return
		if is_darwin; then
			open "$output_file"
		else
			xdg-open "$output_file"
		fi
	}
}
alias pdtex='pdrun tex'
alias pdctex='pdrun ctex'

function pdinit() {
	local doc_type=$1
	[ $# = 1 ] || (abort "Usage: $0 <tex|ctex>") || return

	[ -e ~/.pandoc ] || {
		echo "NOTE: Automatically linking ~/.pandoc to $dotfiles_dir/.pandoc"
		ln -s "$dotfiles_dir/.pandoc" $dotfiles_dir
	}
	local metafile=~/.pandoc/"$doc_type"-meta.yaml
	[ -f $metafile ] || {
		echo ERROR: $metafile "doesn't exist!"
		return 1
	}
	cp $metafile .
	cp "$dotfiles_dir"/latex/mathsyms.tex .
}

# Note: It'd be hard to run AppImages in docker containers
if is_in_docker || command_not_exists nv; then
	alias nv='nvim'
fi

alias -g :A='| awk'
alias -g :B='| base64'
alias -g :BD='| base64 -d'
alias -g :BDO='| base64 -d -w0'
alias -g :BO='| base64 -w0'
alias -g :C='| cut'
alias -g :F='| fzf'
alias -g :FP='| fzfp'
alias -g :G='| grep'
alias -g :GE='| grep -E'
alias -g :GI='| grep -i'
alias -g :H='| head'
alias -g :HD='| hexdump'
alias -g :HDC='| hexdump -C'
alias -g :L='| less'
alias -g :NE='2> /dev/null'
alias -g :NI='< /dev/null'
alias -g :NO='> /dev/null'
alias -g :NOE='&> /dev/null'
alias -g :S='| sort'
alias -g :SD='| sed'
alias -g :ST='| sudo tee'
alias -g :T='| tail'
alias -g :TC='| termcopy'
alias -g :U='| uniq'
alias -g :WL='| wc -l'

# vi-mode cursor shape - taken from ohmyzsh's vi-mode plugin {{{
typeset -g VI_KEYMAP=main

function _vi-mode-set-cursor-shape-for-keymap() {
	# https://vt100.net/docs/vt510-rm/DECSCUSR
	local _shape=0
	case "${1:-${VI_KEYMAP:-main}}" in
		main)    _shape=6 ;; # vi insert: line
		viins)   _shape=6 ;; # vi insert: line
		isearch) _shape=6 ;; # inc search: line
		command) _shape=6 ;; # read a command name
		vicmd)   _shape=2 ;; # vi cmd: block
		visual)  _shape=2 ;; # vi visual mode: block
		viopp)   _shape=0 ;; # vi operation pending: blinking block
		*)       _shape=0 ;;
	esac
	printf $'\e[%d q' "${_shape}"
}

# Updates editor information when the keymap changes.
function zle-keymap-select() {
	# update keymap variable for the prompt
	typeset -g VI_KEYMAP=$KEYMAP
	_vi-mode-set-cursor-shape-for-keymap "${VI_KEYMAP}"
}
zle -N zle-keymap-select

# These "echoti" statements were originally set in lib/key-bindings.zsh
# Not sure the best way to extend without overriding.
function zle-line-init() {
	local prev_vi_keymap
	prev_vi_keymap="${VI_KEYMAP:-}"
	typeset -g VI_KEYMAP=main
	(( ! ${+terminfo[smkx]} )) || echoti smkx
	_vi-mode-set-cursor-shape-for-keymap "${VI_KEYMAP}"
}
zle -N zle-line-init

function zle-line-finish() {
	typeset -g VI_KEYMAP=main
	(( ! ${+terminfo[rmkx]} )) || echoti rmkx
	_vi-mode-set-cursor-shape-for-keymap default
}
zle -N zle-line-finish
# }}}

bindkey -M vicmd '^X^E' edit-command-line  # Make ^X^E work also in vi-mode
bindkey '\e' vi-cmd-mode  # Esc
KEYTIMEOUT=1  # 10ms for key sequences

bindkey -s '\el' '^Q l\n'  # Alt-l
bindkey -s '\eg' '^Q gst\n'  # Alt-g
bindkey -s '\ed' '^Q gd\n'  # Alt-d
bindkey -s '\eD' '^Q gds\n'  # Alt-D
bindkey -s '\eH' ' --help\n\e[A\ew'  # Alt-H
bindkey -s '^g' '^Q fg\n'  # Ctrl-g
bindkey '\e[1;3D' vi-backward-blank-word  # Alt-left
bindkey '\e[1;3C' vi-forward-blank-word  # Alt-right
bindkey '^X^Z' undo  # Also ^x^u by default
bindkey '^U' backward-kill-line

autoload -U zmv
alias zcp='zmv -C'
alias zlns='zmv -Ls'

# https://zsh-users.zsh.narkive.com/yvT5H9G6/delete-everything-to-the-left-until-next-whitespace
autoload -U backward-kill-word-match
zle -N backward-kill-word-whitespace backward-kill-word-match
zstyle ':zle:backward-kill-word-whitespace' word-style whitespace
bindkey '\ew' backward-kill-word-whitespace  # Alt-w

# https://unix.stackexchange.com/questions/258656/how-can-i-have-two-keystrokes-to-delete-to-either-a-slash-or-a-word-in-zsh
backward-kill-dir () {
	local WORDCHARS='~!@#$%^&*()_+.?-="`\[](){}<>:;'"'"
	zle backward-kill-word
	zle -f kill
}
zle -N backward-kill-dir
bindkey '^[^?' backward-kill-dir  # Alt-backspace

if [ -n "${USE_GPG_SSH_AGENT}" ]; then
	gpg_agent_ssh_socket=$(gpgconf --list-dir agent-ssh-socket 2> /dev/null)
	if [ -e "${gpg_agent_ssh_socket}" ]; then
		export SSH_AUTH_SOCK=${gpg_agent_ssh_socket}
	fi
fi

if [ -z "${DONT_AUTO_SET_PROXY}" ]; then
	proxy_port=${AUTO_SET_PROXY_PORT:-8123}
	hosts_file=${HOSTALIASES:-/etc/hosts}
	if [ -f "${hosts_file}" ]; then
		if grep -Eo '^[^#]+\s+proxy(\s|$)' "${hosts_file}" &> /dev/null; then
			set_http_proxy proxy:$proxy_port __no_set_x
			# Also set UP_PROXY if it's not set by the user
			UP_PROXY=${UP_PROXY:-proxy:$proxy_port}
		fi
	fi
fi

SDKMAN_DIR=${SDKMAN_DIR:-~/.sdkman}
if [ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]; then
	export SDKMAN_DIR
	source "$SDKMAN_DIR/bin/sdkman-init.sh"
fi

if command_exists pyenv; then
	eval "$(pyenv init --path)"
	eval "$(pyenv init -)"
	# The following is disabled for better prompt performance
	# TODO: Further check if not eval'ing this causes problems
	# https://github.com/pyenv/pyenv-virtualenv/blob/master/README.md#activate-virtualenv
	# https://github.com/pyenv/pyenv-virtualenv/issues/259
	# if type -p pyenv-virtualenv-init &> /dev/null; then
		# eval "$(pyenv virtualenv-init -)"
	# fi
fi

# https://github.com/nvm-sh/nvm/issues/1978#issuecomment-694919953
function setup-nvm() {
	local install_dir=$1
	[ -s "$install_dir/nvm.sh" ] || return 1
	export NVM_DIR="$HOME/.nvm"
	nvm_cmds=(nvm node npm npx yarn corepack)
	for cmd in $nvm_cmds ; do
		alias $cmd="echo 'NOTE: Initialising nvm...'; unalias $nvm_cmds && unset nvm_cmds && . $install_dir/nvm.sh && rehash && $cmd"
	done
}
setup-nvm "$HOME/.nvm" || setup-nvm /usr/local/opt/nvm || true

function direnv-enable-hook() {
	if ! type _direnv_hook &>/dev/null; then
		eval "$(direnv hook zsh)"
	fi
}
alias direnv-enable=direnv-enable-hook
function direnv-disable-hook() {
	if type _direnv_hook &>/dev/null; then
		unset -f _direnv_hook
	fi
}
function direnv-disable() {
	local unload_cmd=
	if [ -n "$DIRENV_DIFF" ]; then
		# We don't expect /dev to have a .envrc
		unload_cmd=$( direnv-disable-hook; cd /dev; direnv export zsh )
	fi
	direnv-disable-hook
	eval "$unload_cmd"
}

export VOLTA_HOME="$HOME/.volta"
[ -d "$VOLTA_HOME" ] && add_to_path "$VOLTA_HOME/bin" prepend

conda_exe=$HOME/miniconda3/condabin/conda
if [ -x "$conda_exe" ]; then
	export CONDA_AUTO_ACTIVATE_BASE=false
	eval "$("$conda_exe" shell.zsh hook)"
fi

if type -f __lzshrc_final_hook &> /dev/null; then
	__lzshrc_final_hook
fi

# zprof
