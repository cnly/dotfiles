#! /usr/bin/env bash
# ^ Use this instead of /bin/bash for mac users with newer bash installed using brew
# We need at least bash 4.4 https://stackoverflow.com/a/46480987

BASEDIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "$BASEDIR/shlib/detect-env.sh"
source "$BASEDIR/shlib/utils.sh"

set -ex

avail_funcs() {
	grep -E '^function (.+)\(\).+' "${BASH_SOURCE[0]}" | sed -E 's|^function (.+)\(\).*|\1|'
}

git_handle_repo(){
	local repo_path=$1
	local clone_url=$2
	local rev=$3
	echo "Creating/updating git repo at ${repo_path}..."
	if [ ! -d "${repo_path}" ]; then
		git clone "${clone_url}" "${repo_path}"
		[ -z "$rev" ] || git -C "${repo_path}" checkout "$rev"
	elif [ -n "$rev" ]; then
		git -C "${repo_path}" fetch --tags
		git -C "${repo_path}" checkout "$rev"
	else
		git -C "${repo_path}" pull --ff-only
	fi
}

ensure_oh_my_zsh_custom() {
	local plugin_part_path=$1  # Example: themes/powerlevel10k
	local git_clone_url=$2
	local git_repo_path="${HOME}/.oh-my-zsh/custom/${plugin_part_path}"
	git_handle_repo "$git_repo_path" "$git_clone_url"
}

function update_oh_my_zsh() {
	# For the meanwhile, oh-my-zsh plugins are maintained manually
	local ZSH=~/.oh-my-zsh
	env ZSH=$ZSH sh $ZSH/tools/upgrade.sh || true  # Comes from `which upgrade_oh_my_zsh`

	ensure_oh_my_zsh_custom "themes/powerlevel10k" "https://github.com/romkatv/powerlevel10k.git"
	ensure_oh_my_zsh_custom "plugins/zsh-autosuggestions" "https://github.com/zsh-users/zsh-autosuggestions"
	ensure_oh_my_zsh_custom "plugins/zsh-syntax-highlighting" "https://github.com/zsh-users/zsh-syntax-highlighting"
	ensure_oh_my_zsh_custom "plugins/zlong_alert" "https://github.com/kevinywlui/zlong_alert.zsh"
	ensure_oh_my_zsh_custom "plugins/fzf-tab" "https://github.com/Aloxaf/fzf-tab"
}

function update_tmux() {
	git_handle_repo ~/.tmux/plugins/tpm "https://github.com/tmux-plugins/tpm"
	tmux source ~/.tmux.conf || true
	TMUX_PLUGIN_MANAGER_PATH=~/.tmux/plugins/ ~/.tmux/plugins/tpm/bin/install_plugins
	TMUX_PLUGIN_MANAGER_PATH=~/.tmux/plugins/ ~/.tmux/plugins/tpm/bin/update_plugins all
}

function update_vim_plugins() {
	git_handle_repo ~/.vim/vim-plug/autoload "https://github.com/junegunn/vim-plug.git"
	# https://github.com/junegunn/vim-plug/issues/980
	nvim --headless -u NONE +'silent! source ~/.vimrc' +'set nolazyredraw' \
		+PlugClean! +qall
	nvim --headless -u NONE +'silent! source ~/.vimrc' +'set nolazyredraw' \
		+'PlugUpdate --sync' +qall
	# https://github.com/neovim/neovim/issues/13267
	nvim --headless -u NONE +'silent! source ~/.vimrc' +'set nolazyredraw' \
		+'runtime! plugin/rplugin.vim | UpdateRemotePlugins' +qall
}

function update_coc_nvim() {
	coc_extensions=(
		coc-highlight
		coc-ultisnips
		coc-json
		# coc-pyright
		coc-basedpyright
		coc-clangd
		coc-go
		coc-java
		coc-html
		coc-vimtex
		coc-texlab
		coc-vimlsp
		coc-diagnostic
	)
	nvim --headless \
		+"CocInstall -sync ${coc_extensions[*]}" \
		+'CocUpdateSync' +qall
}

function update_nvim_treesitter() {
	local tmpfile=$(mktemp)
	nvim --headless '+set nomore' '+TSUpdate' > "$tmpfile" 2>&1 &
	local nvim_pid=$!
	local -; set +x;
	echo "Waiting for nvim-treesitter parsers to update..."
	local progress_pattern='\[[0-9]+/[0-9]+\] '
	local success=
	sleep 3  # For robustness
	for t in {3..180}; do  # Wait for at most 3 minutes
		# shellcheck disable=SC2016
		if $grep_exe -qE "$progress_pattern" "$tmpfile" && \
			$grep_exe -oE "$progress_pattern" "$tmpfile" | \
			tail -n 1 | \
			$sed_exe -E 's|\[(.+)/(.+)\]|\1 \2|' | \
			$awk_exe '{exit $1 == $2 ? 0 : 1}'; then
			printf "Tree-sitter parsers updated in %d seconds: " "$t"
			$grep_exe -oE "$progress_pattern" "$tmpfile" | tail -n 1
			success=1
			break
		elif $grep_exe -qF 'All parsers are up-to-date' "$tmpfile" &&
			$grep_exe -qvE "$progress_pattern" "$tmpfile"; then
			echo "Tree-sitter parsers already up-to-date."
			success=1
			break
		fi
		sleep 1
	done
	[ -n "$success" ] || echo 'Timed out waiting for nvim-treesitter updates'
	kill $nvim_pid
}

function update_vim_go() {
	vim +GoUpdateBinaries +qall
	[ -z "$dotfiles_go_clean_cache" ] || go clean -cache -modcache
}

function update_pip_modules() {
	local python=/usr/bin/python3
	local venv_dir="$BASEDIR"/installed/venv
	[ -d "$venv_dir" ] || {
		mkdir -p "$BASEDIR"/installed
		$python -m venv "$venv_dir"
	}
	# shellcheck disable=SC2086
	$venv_dir/bin/pip install --upgrade \
		pynvim compdb yq
	[ -z "$dotfiles_pip_clean_cache" ] || rm -rf ~/.cache/pip
}

function update_gdb_tools() {
	command_exists gdb || {
		echo 'Skipping as gdb is not installed.'
		return 0
	}

	local pwndbg_rev=
	local platform_os=$(platform_os_version)
	if [ "$platform_os" = 'linux-debian-10' ] || [ "$platform_os" = 'linux-ubuntu-18.04' ]; then
		pwndbg_rev=debian10-final
		echo 'Using old pwndbg version for Debian 10 / Ubuntu 18.04.'
	fi

	local tools_dir="$BASEDIR"/installed/gdb-tools
	[ -d "$tools_dir" ] || mkdir -p "$tools_dir"
	git_handle_repo "$tools_dir"/peda https://github.com/longld/peda.git
	git_handle_repo "$tools_dir"/pwndbg https://github.com/pwndbg/pwndbg.git $pwndbg_rev
	git_handle_repo "$tools_dir"/gef https://github.com/hugsy/gef.git

	pushd "$tools_dir"/pwndbg
	if command_exists apt-get; then
		# Find the correct python version used by gdb and install it with dev headers and pip
		PYVER=$(gdb -batch -q --nx -ex 'pi import platform; print(".".join(platform.python_version_tuple()[:2]))')
		is_root || sudo_arg=sudo
		$sudo_arg apt-get install -y python"${PYVER}"-dev
	fi
	./setup.sh --update
	popd
}

invoke_fn() {
	local -; set -x;
	$1 2> >("$sed_exe" --unbuffered "s|^|[\x1B[0;31m$1\x1B[0m] |") > >("$sed_exe" --unbuffered "s|^|[$1] |")
}

main() {
	make -C "${BASEDIR}" -j all
}

PATH=~/.local/bin:$DEFAULT_BREW_BIN_DIR:$PATH

set +x
if [ "$#" != 0 ]; then
	if [ "$1" = 'help' ]; then
		set +x
		echo -e '\nAvailable functions:'
		avail_funcs | sed 's/^/    /'
		exit 0
	fi

	IFS=', ' read -r -a skips <<< "${dotfiles_update_skip:-}"
	for func in "$@"; do
		func=$(echo "$func" | tr '-' '_')
		if ! typeset -f "${func}" > /dev/null; then
			func="update_${func}"
		fi
		# shellcheck disable=SC2076
		if [[ " ${skips[*]} " =~ " ${func} " ]]; then
			echo "Skipping function ${func}..."
			continue
		fi
		echo "Running function ${func}..."
		invoke_fn "$func"
	done
else
	if commands_exist apt-get sudo; then  # Currently for gdb tools only
		sudo true || exit 1
	fi
	main
	echo 'Everything updated.'
fi

# vim: set fdm=marker:
