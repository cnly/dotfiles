FasdUAS 1.101.10   ��   ��    k             l     ��  ��      References     � 	 	    R e f e r e n c e s   
  
 l     ��  ��    Y S https://apple.stackexchange.com/questions/212813/open-text-file-from-finder-in-vim     �   �   h t t p s : / / a p p l e . s t a c k e x c h a n g e . c o m / q u e s t i o n s / 2 1 2 8 1 3 / o p e n - t e x t - f i l e - f r o m - f i n d e r - i n - v i m      l     ��  ��    ~ x https://apple.stackexchange.com/questions/286943/how-do-i-write-a-script-that-opens-an-iterm2-window-and-runs-a-command     �   �   h t t p s : / / a p p l e . s t a c k e x c h a n g e . c o m / q u e s t i o n s / 2 8 6 9 4 3 / h o w - d o - i - w r i t e - a - s c r i p t - t h a t - o p e n s - a n - i t e r m 2 - w i n d o w - a n d - r u n s - a - c o m m a n d      l     ��  ��    E ? https://gist.github.com/napcs/2d8376e941133ccfad63e33bf1b1b60c     �   ~   h t t p s : / / g i s t . g i t h u b . c o m / n a p c s / 2 d 8 3 7 6 e 9 4 1 1 3 3 c c f a d 6 3 e 3 3 b f 1 b 1 b 6 0 c      l     ��������  ��  ��        l     ��  ��      Notes:     �      N o t e s :     !   l     �� " #��   " / ) -45 error while saving means file locked    # � $ $ R   - 4 5   e r r o r   w h i l e   s a v i n g   m e a n s   f i l e   l o c k e d !  % & % l     �� ' (��   ' ? 9 Exporting the app will cause the default icon to be used    ( � ) ) r   E x p o r t i n g   t h e   a p p   w i l l   c a u s e   t h e   d e f a u l t   i c o n   t o   b e   u s e d &  * + * l     �� , -��   , N H We have manually modified Info.plist to add more supported file formats    - � . . �   W e   h a v e   m a n u a l l y   m o d i f i e d   I n f o . p l i s t   t o   a d d   m o r e   s u p p o r t e d   f i l e   f o r m a t s +  / 0 / l     ��������  ��  ��   0  1 2 1 l     �� 3 4��   3 - ' Backup steps to change the app's icon:    4 � 5 5 N   B a c k u p   s t e p s   t o   c h a n g e   t h e   a p p ' s   i c o n : 2  6 7 6 l     �� 8 9��   8 . ( - Export it as an app in another folder    9 � : : P   -   E x p o r t   i t   a s   a n   a p p   i n   a n o t h e r   f o l d e r 7  ; < ; l     �� = >��   = ; 5 - Use the Nvim droplet.icns to replace the stock one    > � ? ? j   -   U s e   t h e   N v i m   d r o p l e t . i c n s   t o   r e p l a c e   t h e   s t o c k   o n e <  @ A @ l     �� B C��   B ' ! - Delete the "Icon?" file if any    C � D D B   -   D e l e t e   t h e   " I c o n ? "   f i l e   i f   a n y A  E F E l     �� G H��   G #  - Rename the app to Nvim.app    H � I I :   -   R e n a m e   t h e   a p p   t o   N v i m . a p p F  J K J l     ��������  ��  ��   K  L M L i     N O N I      �� P���� 0 runcmd runCmd P  Q�� Q o      ���� 0 cmd  ��  ��   O O      R S R k     T T  U V U r     W X W l   	 Y���� Y I   	�� Z��
�� .Itrmnwwpnull���     **** Z m     [ [ � \ \  E d i t o r��  ��  ��   X o      ���� 0 	newwindow 	newWindow V  ]�� ] O     ^ _ ^ I   ���� `
�� .Itrmsntxnull���     obj ��   ` �� a��
�� 
Text a b     b c b o    ���� 0 cmd   c m     d d � e e    & &   e x i t��   _ n     f g f 1    ��
�� 
Wcsn g o    ���� 0 	newwindow 	newWindow��   S m      h h|                                                                                  ITRM  alis      Macintosh HD               �XN�BD ����	iTerm.app                                                      �����]��        ����  
 cu             Applications  /:Applications:iTerm.app/    	 i T e r m . a p p    M a c i n t o s h   H D  Applications/iTerm.app  / ��   M  i j i l     ��������  ��  ��   j  k l k i     m n m I     �� o��
�� .aevtodocnull  �    alis o o      ���� 0 thefiles theFiles��   n k      p p  q r q r     
 s t s n      u v u 1    ��
�� 
strq v n      w x w 1    ��
�� 
psxp x n      y z y 4    �� {
�� 
cobj { m    ����  z o     ���� 0 thefiles theFiles t o      ���� 0 filename   r  | } | r     ~  ~ b     � � � b     � � � b     � � � b     � � � m     � � � � � *   c l e a r ;   c d   " ` d i r n a m e   � o    ���� 0 filename   � m     � � � � � ( ` " ;   n v i m   " ` b a s e n a m e   � o    ���� 0 filename   � m     � � � � �  ` "  o      ���� 0 cmd   }  ��� � I    �� ����� 0 runcmd runCmd �  ��� � o    ���� 0 cmd  ��  ��  ��   l  � � � l     ��������  ��  ��   �  ��� � i     � � � I     ������
�� .aevtoappnull  �   � ****��  ��   � k      � �  � � � l     �� � ���   � N H  Handle the case where the script is launched without any dropped files    � � � � �     H a n d l e   t h e   c a s e   w h e r e   t h e   s c r i p t   i s   l a u n c h e d   w i t h o u t   a n y   d r o p p e d   f i l e s �  ��� � I     �� ����� 0 runcmd runCmd �  ��� � m     � � � � �  n v i m��  ��  ��  ��       �� � � � ���   � �������� 0 runcmd runCmd
�� .aevtodocnull  �    alis
�� .aevtoappnull  �   � **** � �� O���� � ����� 0 runcmd runCmd�� �� ���  �  ���� 0 cmd  ��   � ������ 0 cmd  �� 0 	newwindow 	newWindow �  h [������ d��
�� .Itrmnwwpnull���     ****
�� 
Wcsn
�� 
Text
�� .Itrmsntxnull���     obj �� � �j E�O��, *��%l UU � �� n���� � ���
�� .aevtodocnull  �    alis�� 0 thefiles theFiles��   � �������� 0 thefiles theFiles�� 0 filename  �� 0 cmd   � ������ � � ���
�� 
cobj
�� 
psxp
�� 
strq�� 0 runcmd runCmd�� ��k/�,�,E�O�%�%�%�%E�O*�k+  � �� ����� � ���
�� .aevtoappnull  �   � ****��  ��   �   �  ����� 0 runcmd runCmd�� *�k+ ascr  ��ޭ