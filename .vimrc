" Note: we need to source this first, at least before has('python3'), because
" we set g:loaded_python3_provider in there
" Thanks to https://stackoverflow.com/a/18734557 for next line
let s:script_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')
exec 'source '.s:script_dir.'/.minivimrc'

" Just keep using the directory used by Vundle
set rtp+=~/.vim/vim-plug

function! MyUpdateRemotePlugins(...)
	" https://github.com/gelguy/wilder.nvim/issues/109
	" Needed to refresh runtime files
	let &rtp=&rtp
	UpdateRemotePlugins
endfunction

call plug#begin('~/.vim/bundle')  " {{{

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'NLKNguyen/papercolor-theme'
Plug 'ntpeters/vim-better-whitespace'
Plug 'majutsushi/tagbar'
Plug 'fatih/vim-go'
Plug 'dhruvasagar/vim-table-mode'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-obsession'
Plug 'scrooloose/nerdcommenter'
" Plug 'scrooloose/nerdtree'
" Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-repeat'
Plug 'svermeulen/vim-cutlass'
Plug 'ronakg/quickr-preview.vim'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'tpope/vim-unimpaired'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'qpkorr/vim-bufkill'
Plug 'tpope/vim-surround'
Plug 'embear/vim-localvimrc'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 't9md/vim-quickhl'
Plug 'easymotion/vim-easymotion'
Plug 'honza/vim-snippets'
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-line'
Plug 'kana/vim-textobj-fold'
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'liquidz/vim-iced', {'for': 'clojure'}
Plug 'liquidz/vim-iced-coc-source', {'for': 'clojure'}
Plug 'lambdalisue/fern.vim'
Plug 'liquidz/vim-iced-fern-debugger', {'for': 'clojure'}
Plug 'kassio/neoterm'
Plug 'haya14busa/vim-asterisk'
Plug 'MTDL9/vim-log-highlighting'
Plug 'wellle/targets.vim'  " Provides various textobjects
Plug 'rbong/vim-flog'
Plug 'rhysd/committia.vim'
Plug 'lervag/vimtex'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'vim-pandoc/vim-pandoc'
Plug 'gelguy/wilder.nvim', { 'do': function('MyUpdateRemotePlugins') }
Plug 'andymass/vim-matchup'  " Can use treesitter to accelerate
if has('python3')
	Plug 'SirVer/ultisnips'
endif
Plug 'Raimondi/delimitMate'
Plug 'ruanyl/vim-gh-line'
Plug 'unblevable/quick-scope'
Plug 'tpope/vim-sleuth'
Plug 'ojroques/vim-oscyank'
Plug 'wesQ3/vim-windowswap'
Plug 'tommcdo/vim-ninja-feet'
Plug 'github/copilot.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'LnL7/vim-nix'
Plug 'Konfekt/vim-sentence-chopper'
Plug 'mbbill/undotree'
if has('nvim-0.5')
	" Plug 'windwp/nvim-autopairs'

	Plug 'nvim-lua/plenary.nvim'
	Plug 'nvim-tree/nvim-web-devicons'

	Plug 'nvim-pack/nvim-spectre'
	Plug 'lukas-reineke/indent-blankline.nvim'
	" Plug 'nanozuki/tabby.nvim'  " Tabby can't scroll (May 2024)
endif
if has('nvim-0.8')
	Plug 'nvim-tree/nvim-tree.lua'
	" Treesitter and friends
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	Plug 'nvim-treesitter/nvim-treesitter-textobjects'
	Plug 'windwp/nvim-ts-autotag'  " Can rename html tags
	" Plug 'mrjones2014/nvim-ts-rainbow'
	Plug 'https://gitlab.com/HiPhish/rainbow-delimiters.nvim'
	" Plug 'romgrk/nvim-treesitter-context'
	Plug 'gsuuon/tshjkl.nvim'
endif
if has('nvim-0.9')
	Plug 'lewis6991/gitsigns.nvim'
endif
if has('nvim-0.10')
	Plug 'Bekaboo/dropbar.nvim'
	" Plug 'lewis6991/satellite.nvim'
	Plug 'dstein64/nvim-scrollview'
endif
Plug 'ryanoasis/vim-devicons'  " Always load the vim-devicons as the very last one

call plug#end()  " }}}

" Enable ONLY if TERM is set valid and it is NOT under mosh
" https://github.com/wookayin/dotfiles/blob/0d44f9c24328ccba5e21cf776b33bdef912fbdc6/vim/vimrc#L579-L609
" 24-bit true color: neovim 0.1.5+ / vim 7.4.1799+
function! IsMosh()
	let output = system(shellescape(s:script_dir.'/shlib/is_mosh').' -v')
	if v:shell_error
		return 0
	endif
	return !empty(l:output)
endfunction

function! s:auto_termguicolors(...)
	if !(has('termguicolors'))
		return
	endif

	if (&term == 'xterm-256color' || &term == 'nvim') && !IsMosh()
		set termguicolors
	else
		set notermguicolors
	endif
endfunction

call s:auto_termguicolors()

" ===========================
" Plugin Config Secion Starts
" ===========================

" vim-pandoc and vim-pandoc-syntax {{{
let g:pandoc#modules#disabled = ["folding"]
let g:pandoc#syntax#conceal#use = 0
" hi! link Conceal WarningMsg
" }}}

" vimtex {{{
let g:vimtex_mappings_prefix = '<localleader>'
let g:vimtex_quickfix_open_on_warning = 0
if has('macunix')
	let g:vimtex_view_method = 'skim'
endif
" }}}

" copilot.vim {{{
let g:copilot_filetypes = {
		\ 'conf': v:false,
		\ 'dosini': v:false,
		\ }
" }}}

" wilder.nvim {{{
call wilder#setup({'modes': [':', '/', '?'],
			\ 'accept_key': '<C-l>', 'reject_key': '<C-h>',
			\ 'accept_completion_auto_select': 0})
cmap <expr> <Right> getcmdpos() == len(getcmdline()) + 1 ? "<C-l>" : "<Right>"
" cmap <Down> <Tab>
" cmap <Up> <S-Tab>

" splitjoin.vim {{{
let g:splitjoin_curly_brace_padding = 0
let g:splitjoin_python_brackets_on_separate_lines = 1
" }}}

call wilder#set_option('pipeline', [
			\   wilder#branch(
			\     wilder#cmdline_pipeline({
			\       'fuzzy': 1,
			\     }),
			\     wilder#search_pipeline({
			\       'pattern': 'fuzzy',
			\     }),
			\   ),
			\ ])

call wilder#set_option('renderer', wilder#popupmenu_renderer({
			\   'highlighter': wilder#basic_highlighter(),
			\   'left': [' ', wilder#popupmenu_devicons()],
			\   'right': [' ', wilder#popupmenu_scrollbar()],
			\ }))
" }}}

" vim-asterisk {{{
map *  <Plug>(asterisk-z*)
map #  <Plug>(asterisk-z#)
map g* <Plug>(asterisk-gz*)
map g# <Plug>(asterisk-gz#)
let g:asterisk#keeppos = 1
" }}}

" neoterm {{{
let g:neoterm_default_mod = ':botright'
let g:neoterm_autoscroll = 1
let g:neoterm_auto_repl_cmd = 1
let g:neoterm_autojump = 1
let g:neoterm_automap_keys = '<leader>tt'
nmap <leader>te <Plug>(neoterm-repl-send)
xmap <leader>te <Plug>(neoterm-repl-send)
nmap <leader>tee <Plug>(neoterm-repl-send-line)
noremap ~ :Ttoggle<CR>
" }}}

" vim-iced {{{
let $PATH = $PATH.':'.$HOME.'/.vim/bundle/vim-iced/bin'
let g:iced_enable_default_key_mappings = v:true
let g:iced_default_key_mapping_leader = '<LocalLeader>'
" let g:iced#debug#debugger = 'fern'
" }}}

" fern.vim {{{
" Vim prior to 8.1.0994 does not have required feature thus fern is disabled.
let g:fern_disable_startup_warnings = 1
" }}}

" vim-sexp {{{
let g:sexp_enable_insert_mode_mappings = 0
let g:sexp_mappings = {
			\ 'sexp_swap_list_backward': '<LocalLeader><M-k>',
			\ 'sexp_swap_list_forward': '<LocalLeader><M-j>',
			\ 'sexp_swap_element_backward': '<LocalLeader><M-h>',
			\ 'sexp_swap_element_forward': '<LocalLeader><M-l>',
			\ 'sexp_move_to_next_element_head': '<LocalLeader><M-w>',
			\ 'sexp_indent': '',
			\ 'sexp_indent_top': '',
			\ 'sexp_capture_next_element': '',
			\ 'sexp_capture_prev_element': ''
			\ }
" }}}

" vim-bufkill {{{
let g:BufKillCreateMappings = 0
" Ctrl-4 or Ctrl-\ mapped to alternative buffer (bufkill version)
nnoremap <silent> <C-\> <Cmd>execute(v:count == 0 ? 'BA' : 'b'.v:count)<CR>
nmap <C-4> <C-\>
imap <C-\> <C-O><C-\>
imap <C-4> <C-\>
" }}}

" ultisnips
let g:UltiSnipsExpandTrigger = '<Plug>ZZZZVimrcDisabledUltisnipsExpandTrigger'
let g:UltiSnipsSnippetDirectories = ["UltiSnips", s:script_dir."/vim/UltiSnips"]

" vim-localvimrc {{{
let g:localvimrc_event = "BufRead"
let g:localvimrc_persistent = 1
let g:localvimrc_sandbox = 0
" }}}

" vim-easymotion {{{
let g:EasyMotion_smartcase = 1
nmap s <Plug>(easymotion-overwin-f2)
map S <Plug>(easymotion-prefix)
" }}}

" ntpeters/vim-better-whitespace {{{
let g:better_whitespace_enabled = 1
let g:better_whitespace_filetypes_blacklist = ['floggraph']
augroup vimrc_better_whitespace
	autocmd!
	autocmd ColorScheme * highlight link ExtraWhitespace SpellBad
augroup END
" }}}

" fzf {{{
let $PATH = $HOME.'/.vim/bundle/fzf/bin:'.$PATH
let g:fzf_colors =
			\ { 'fg':      ['fg', 'Normal'],
			\ 'bg':      ['bg', 'Normal'],
			\ 'hl':      ['fg', 'Comment'],
			\ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
			\ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
			\ 'hl+':     ['fg', 'Statement'],
			\ 'info':    ['fg', 'PreProc'],
			\ 'border':  ['fg', 'Ignore'],
			\ 'prompt':  ['fg', 'Conditional'],
			\ 'pointer': ['fg', 'Exception'],
			\ 'marker':  ['fg', 'Keyword'],
			\ 'spinner': ['fg', 'Label'],
			\ 'header':  ['fg', 'Comment'] }

let g:fzf_history_dir = '~/.fzf-history.d'
let g:fzf_layout = { 'window': 'noautocmd tabnew' }

function! s:fzf_build_quickfix_list(lines)
	call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
	copen
endfunction

let g:fzf_action = {
			\ 'ctrl-q': function('s:fzf_build_quickfix_list'),
			\ 'ctrl-t': 'tab split',
			\ 'ctrl-s': 'split',
			\ 'ctrl-v': 'vsplit' }

augroup vimrc_fzf
	autocmd! FileType fzf
	autocmd  FileType fzf setlocal nonumber norelativenumber laststatus=0
				\ | autocmd BufLeave <buffer> set laststatus=2
	autocmd FileType fzf silent! tnoremap <buffer> <Esc> <Esc>
augroup END

function! s:fzf_all_open_files_escaped()
	return map(
			  \ filter(range(1, bufnr('$')),
			  \ 	'buflisted(v:val) && getbufvar(v:val, "&ft") != "qf" && filereadable(fnamemodify(bufname(v:val), ":p"))'
			  \ 	),
			  \ 'fzf#shellescape(bufname(v:val))'
			  \ )
endfunction

function! s:fzf_ag(query, ag_opts_list, bang)
	let ag_opts = !empty(a:ag_opts_list) ? join(a:ag_opts_list, ' ') : '--hidden --ignore .git'
	let ag_opts = ag_opts." --color-path='0;32' --color-line-number='0;33'"
	call fzf#vim#ag(a:query, ag_opts,
				\ fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'up:50%', 'alt-p'), a:bang)
endfunction

function! s:fzf_ag_raw(bang, ...)
	call fzf#vim#ag_raw(join(a:000, ' '),
			  \ fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'up:50%', 'alt-p'), a:bang)
endfunction

command! -bang -nargs=+ -complete=dir AgRaw call s:fzf_ag_raw(<bang>0, <f-args>)
command! -bang -nargs=? -complete=dir Files
			\ call fzf#vim#files(<q-args>, fzf#vim#with_preview('right:50%', 'alt-p'), <bang>0)
command! -bang -nargs=* -complete=dir Ag
			\ call s:fzf_ag(get([<f-args>], 0, ''), [<f-args>][1:], <bang>0)
command! -bang -nargs=* -complete=buffer Buffers
			\ call fzf#vim#buffers(<q-args>, fzf#vim#with_preview('up:50%', 'alt-p'), <bang>0)
command! -bang -nargs=* Lines0
			\ call fzf#vim#lines(<q-args>, <bang>0)
command! -bang Lines
			\ call s:fzf_ag(' ',
			\ 	['--silent --filename', fzf#shellescape('^(?=.)'),
			\		join(s:fzf_all_open_files_escaped(), ' ')],
			\ <bang>0)
command! -bar -bang -nargs=? Maps call fzf#vim#maps(get([<f-args>], 0, 'n'), 0)

nnoremap <silent> GP :Files<CR>
nnoremap <silent> GA :Ag<CR>
nnoremap <leader>a :<C-u>AgRaw<space>
nnoremap <silent> GB :let g:fzf_buffers_jump = 1<CR>:Buffers<CR>
nnoremap <silent> Gb :let g:fzf_buffers_jump = 0<CR>:Buffers<CR>
nnoremap <silent> GH :call fzf#vim#history(fzf#vim#with_preview('up:50%', 'alt-p'))<CR>
nnoremap <silent> G: :History:<CR>
nnoremap <silent> GM :Marks<CR>
nnoremap <silent> GL :Lines<CR>
nnoremap <silent> Gl :Lines0<CR>
nnoremap <silent> <C-p> <Cmd>Maps<CR>
" }}}

" quickr-preview {{{
let g:quickr_preview_keymaps=0  " Don't map <leader><space>
" }}}

" coc.nvim {{{
if has_key(g:plugs, 'coc.nvim')

	let s:langservers = {
		\ 'clojure-lsp': {
		\   "command": "clojure-lsp",
		\   "filetypes": ["clojure"],
		\   "disableCompletion": v:true,
		\   "rootPatterns": ["project.clj"],
		\   "additionalSchemes": ["jar", "zipfile"],
		\   "initializationOptions": {}
		\ }
		\}
	if executable('nixd')
		let s:langservers['nixd'] = {
			\ 'command': 'nixd',
			\ 'filetypes': ['nix']
			\ }
	endif
	call coc#config('languageserver', s:langservers)

	call coc#config('coc.source.iced.enable', v:true)
	if exists('$http_proxy')
		call coc#config('http.proxy', substitute($http_proxy, '\vhttp:|/', '', 'g'))
	elseif exists('$HTTP_PROXY')
		call coc#config('http.proxy', substitute($HTTP_PROXY, '\vhttp:|/', '', 'g'))
	endif
	if has('nvim-0.10')
		call coc#config('floatFactory.floatConfig', {
				\ 'border': v:true,
				\ 'highlight': 'NormalFloat',
				\ 'borderhighlight': 'FloatBorder'
			  \ })
	endif
endif

let g:coc_disable_startup_warning = 1
let g:coc_enable_locationlist = 0

" " https://vi.stackexchange.com/a/25609
" inoremap <silent> <expr> <CR> pumvisible() ? (complete_info().selected == -1 ? '<C-y><CR>' : '<C-y>') : '<CR>'

function s:CocConfirmAndEnter()
	call coc#pum#confirm()
	return "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
endfunction

inoremap <silent> <expr> <CR>
	\ coc#pum#visible() ?
	\ (coc#pum#info()['index'] == -1 ? <SID>CocConfirmAndEnter() : coc#pum#confirm())
	\ : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
inoremap <silent> <expr> <TAB> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
inoremap <silent> <expr> <down> coc#pum#visible() ? coc#pum#next(1) : "\<down>"
inoremap <silent> <expr> <up> coc#pum#visible() ? coc#pum#prev(1) : "\<up>"

inoremap <silent> <expr> <C-Space> coc#refresh()
nmap <silent> <F7> <Plug>(coc-references)
nmap <silent> <F6> <Plug>(coc-rename)
noremap Q <Cmd>call CocActionAsync('doHover')<CR>
nmap <silent> <F8> <Plug>(coc-implementation)
nmap <silent> <F4> <Plug>(coc-definition)
nmap <silent> <F5> <Plug>(coc-type-definition)
map <silent> <leader>q <Plug>(coc-format-selected)
nmap <silent> <leader>qq <Plug>(coc-format-selected)l
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nnoremap GS <Cmd>CocList symbols<CR>
nno <silent> # <Cmd>CocCommand document.jumpToNextSymbol<CR>
nno <silent> <M-#> <Cmd>CocCommand document.jumpToPrevSymbol<CR>
nnoremap yoI <Cmd>CocCommand document.toggleInlayHint<CR>

amenu PopUp.-Coc- <Nop>
amenu PopUp.Quick\ Fix <Plug>(coc-fix-current)
amenu PopUp.Code\ Actions <Plug>(coc-codeaction)
amenu PopUp.Sort\ Imports <Cmd>CocCommand editor.action.organizeImport<CR>
amenu PopUp.Coc\ Workspace\ Output <Cmd>CocCommand workspace.showOutput<CR>

" Override .minivimrc
nnoremap <leader><leader> <Cmd>call coc#float#close_all() \| echo GetStatus()<CR>

function s:MaybeShowCocSignatureHelp()
	if &buftype !=# '' && &buftype !=# 'acwrite' | return | endif  " Coc won't attach this document
	if coc#float#has_float() | return | endif
	call CocActionAsync('showSignatureHelp')
endfunction

function! s:MaybeGetCocFadeOutFgArg(mode)
	let l:colour = synIDattr(synIDtrans(hlID('CocFadeOut')), 'fg', a:mode)
	return empty(l:colour) ? '' : a:mode.'fg=' . l:colour
endfunction

augroup vimrc_coc_nvim
	autocmd!
	autocmd ColorScheme * highlight link CocErrorSign error
	autocmd ColorScheme * highlight link CocWarningSign SpellCap
	autocmd ColorScheme * highlight link CocInfoSign todo
	autocmd ColorScheme * highlight link CocHintSign todo
	autocmd ColorScheme * highlight link CocFadeOut LineNr
	autocmd ColorScheme * exec 'highlight CocInlayHint gui=bold cterm=bold' .
		\ ' ' . s:MaybeGetCocFadeOutFgArg('gui') .
		\ ' ' . s:MaybeGetCocFadeOutFgArg('cterm')
	autocmd ColorScheme * if g:theme != 'dark' | highlight CocHighlightText ctermbg=254 guibg=#e4e4e4 | endif

	autocmd User CocLocationsChange call setloclist(0, g:coc_jump_locations) | execute('lopen')

	autocmd CursorHold * silent! call CocActionAsync('highlight')
	autocmd User CocJumpPlaceholder call s:MaybeShowCocSignatureHelp()
	autocmd CursorHoldI * silent! call s:MaybeShowCocSignatureHelp()
augroup END
" }}}

" indent-blankline {{{
augroup vimrc_indent_blankline
	autocmd!
	autocmd ColorScheme * highlight link IblIndent Whitespace
augroup END
" }}}

" vim-bookmarks {{{
let g:bookmark_no_default_key_mappings = 1
let g:bookmark_save_per_working_dir = 1
augroup vimrc_vim_bookmarks
	autocmd!
	autocmd ColorScheme * highlight BookmarkSign ctermfg=33 guifg=#0087ff
augroup END
nmap <Leader>mm <Plug>BookmarkToggle
nmap <Leader>ma <Plug>BookmarkAnnotate
nmap <Leader>ms <Plug>BookmarkShowAll
nmap <Leader>mn <Plug>BookmarkNext
nmap <Leader>mp <Plug>BookmarkPrev
" }}}

" vim-go {{{
let g:go_autodetect_gopath = 1
" let g:go_gopls_enabled = 0
let g:go_code_completion_enabled = 0
let g:go_fmt_autosave = 0
let g:go_echo_go_info = 0
" }}}

" nerdtree {{{
" function! SmartNERDTreeFind()
	" if &filetype == 'nerdtree'
		" execute('wincmd p')
	" endif
	" execute(':NERDTreeFind')
" endfunction
" noremap <F1> :NERDTreeToggle<CR>
" noremap <leader>! :call SmartNERDTreeFind()<CR>
" augroup vimrc_nerdtree
	" autocmd!
	" autocmd FileType nerdtree nmap <silent> <buffer> <C-v> s
	" autocmd FileType nerdtree nmap <silent> <buffer> <C-s> i
" augroup END
" }}}

" nvim-tree {{{
function! SmartNvimTreeFindFile()
	if &filetype == 'NvimTree'
		execute('wincmd p')
	endif
	execute(':NvimTreeFindFile!')
endfunction
noremap <silent> <F1> <Cmd>NvimTreeToggle<CR>
noremap <silent> <leader>! <Cmd>call SmartNvimTreeFindFile()<CR>
" }}}

" nerdtree-git-plugin {{{
let g:NERDTreeGitStatusIndicatorMapCustom = {
			\ 'Modified'  :'✹',
			\ 'Staged'    :'✚',
			\ 'Untracked' :'✭',
			\ 'Renamed'   :'➜',
			\ 'Unmerged'  :'═',
			\ 'Deleted'   :'✖',
			\ 'Dirty'     :'✗',
			\ 'Ignored'   :'☒',
			\ 'Clean'     :'✔︎',
			\ 'Unknown'   :'?',
			\ }
" }}}

" tagbar {{{
let g:tagbar_autofocus = 1
let g:tagbar_sort = 0
let g:tagbar_left = 1
let g:tagbar_width = 35
let g:tagbar_map_showproto = '<leader><leader>'
noremap <F12> :TagbarToggle<CR><C-w>=
noremap <leader>+ :TagbarOpen fj<CR><C-w>=
" }}}

" vim-visual-multi {{{
let g:VM_case_setting = 'sensitive'
let g:VM_mouse_mappings = 1
let g:VM_leader = '\'
let g:VM_maps = {}
let g:VM_maps["Undo"] = 'u'
let g:VM_maps["Redo"] = '<C-r>'
" }}}

" NERDCommenter {{{
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDCommentWholeLinesInVMode = 1
" let g:NERDCommentEmptyLines = 1
" TODO: How to make this plugin toggle *sexy* comments by default?
xmap <C-/> <Plug>NERDCommenterToggle
map <C-/> <Plug>NERDCommenterToggle
imap <C-/> <C-o><Plug>NERDCommenterToggle
" ^_ is also Ctrl-/
xmap  <Plug>NERDCommenterToggle
map  <Plug>NERDCommenterToggle
imap  <C-o><Plug>NERDCommenterToggle
let g:NERDCustomDelimiters = {
			\ 'clojure': { 'left': ';;' },
			\ 'python': { 'left': '#', 'leftAlt': '#' },
			\ 'systemd': { 'left': '#' },
			\ 'dosini': { 'left': '#' },
			\ }
" }}}

" vim-airline {{{
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
let g:airline_skip_empty_sections = 1

let g:airline#extensions#whitespace#mixed_indent_algo = 1
let g:airline#extensions#whitespace#trailing_format = '[󱁐 %s]'
let g:airline#extensions#whitespace#mixed_indent_format = '[󰉶 %s]'
let g:airline#extensions#whitespace#mixed_indent_file_format = '[󰉶  %s]'
let g:airline#extensions#whitespace#conflicts_format = '[󰞇 %s]'
let g:airline#extensions#whitespace#long_format = '[󰵴 %s]'

let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

let airline#extensions#coc#error_symbol = ' '
let airline#extensions#coc#warning_symbol = ' '

let g:airline#extensions#branch#enabled = 0

let g:airline_skip_empty_sections = 1
let g:airline_highlighting_cache = 1

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_tab_count = 0
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_splits = 0  " The thing on the far right end
let g:airline#extensions#tabline#tab_min_count = 2
let g:airline#extensions#tabline#alt_sep = 0
" https://www.w3.org/TR/xml-entity-names/025.html
let g:airline#extensions#tabline#left_sep = '▎'
let g:airline#extensions#tabline#left_alt_sep = '┃'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" Using the example from airline.txt, we set the tab title to the buffer name
" *in the active window*
" See also: https://github.com/vim-airline/vim-airline/blob/ff0f9a45a5d81d2c8aa67601c264b18c4fe26b15/autoload/airline/extensions/tabline.vim#L209
let g:airline#extensions#tabline#tabtitle_formatter = 'MyTabTitleFormatter'
function MyTabTitleFormatter(n)
	let buflist = tabpagebuflist(a:n)
	let winnr = tabpagewinnr(a:n)
	let bufnr = buflist[winnr - 1]
	return airline#extensions#tabline#get_buffer_name(bufnr, buflist)
endfunction

let g:airline_left_sep = ''
let g:airline_left_alt_sep = '│'
let g:airline_right_sep = ''
let g:airline_right_alt_sep = '│'
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.whitespace = ''

" Default section config in autoload/airline/init.vim, function airline#init#sections()
" For section names see :h airline-intro

function! s:airline_override_sections()

	" Create section a that shows bufnr when window is active
	function! AirlineCustomBufNrActiveFunc()
		return bufnr('%')
	endfunction
	call airline#parts#define_function('bufnr', 'AirlineCustomBufNrActiveFunc')
	let g:airline_section_a = airline#section#create_left(['bufnr', 'crypt', 'paste', 'keymap', 'spell', 'capslock', 'xkblayout', 'iminsert'])

	" Create section c that shows bufnr when window is inactive
	function! AirlineCustomBufNrInactiveFunc()
		return exists('w:airline_active') && w:airline_active ? '' : bufnr('%').g:airline_symbols.space
	endfunction
	call airline#parts#define_function('bufnr_inactive', 'AirlineCustomBufNrInactiveFunc')
	let g:airline_section_c = airline#section#create(['%<', 'bufnr_inactive',
				\ exists("+autochdir") && &autochdir == 1 ? 'path' : 'file',
				\ g:airline_symbols.space, 'readonly', 'coc_status', 'lsp_progress'])

	" Don't show filetype
	let l:section_x_parts = ['bookmark', 'scrollbar', 'taglist', 'vista', 'gutentags', 'gen_tags', 'omnisharp', 'grepper', 'custom_curr_func']
	if !exists(':CocCommand')
		call add(l:section_x_parts, 'tagbar')
	endif
	let g:airline_section_x = airline#section#create_right(l:section_x_parts)

	" Create section z with the percentage, obsession, and some spaces removed
	call airline#parts#define('linenr', {
		\ 'raw': '%l',
		\ 'accent': 'bold'})
	call airline#parts#define('maxlinenr', {
		\ 'raw': '/%L',
		\ 'accent': 'bold'})
	let g:airline_section_z = airline#section#create(['windowswap', 'linenr', 'maxlinenr', ':%v'])

endfunction

augroup vimrc_airline
	autocmd!
	autocmd User AirlineAfterInit call s:airline_override_sections()
augroup END

" Make section a non-bold in normal and bold in insert
" Also use a more sensible color for the error indicator
let g:airline_theme_patch_func = 'AirlineThemePatch'
function! AirlineThemePatch(palette)
	for mode in keys(a:palette)
		" [4]: Mode accent
		if has_key(a:palette[mode], 'airline_a')
			let a:palette[mode]['airline_a'][4] = mode =~ '^insert' ? 'bold' : 'none'
		endif
		let mae = get(a:palette[mode], 'airline_error', [])
		if get(mae, 0) == '#000000' && get(mae, 1) == '#990000'  " Airline defaults
			let a:palette[mode]['airline_error'] = [ '#ffffff', '#d70000', 15, 161 ]
		endif
	endfor
endfunction
" }}}

" vim-quickhl {{{
nmap <silent> + <Plug>(quickhl-manual-this-whole-word)
nmap <silent> 2+ viW<Plug>(quickhl-manual-this)
nmap <silent> -+ <Plug>(quickhl-manual-reset)
xmap <silent> + <Plug>(quickhl-manual-this)
" }}}

" vim-cutlass {{{
call MapFastKeycode('<A-x>', "\ex")
call MapFastKeycode('<A-d>', "\ed")
call MapFastKeycode('<A-D>', "\eD")
call MapFastKeycode('<A-c>', "\ec")
call MapFastKeycode('<A-p>', "\ep")
nnoremap <A-x> x
xnoremap <A-x> x
nnoremap <A-d> d
xnoremap <A-D> D
nnoremap <A-D> D
xnoremap <A-d> d
nnoremap <A-c> c
xnoremap <A-c> c
" }}}

" vim-easy-align {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}

" vim-obsession {{{
nnoremap <silent> q: :let g:obsession_no_bufenter=1<CR>q::let g:obsession_no_bufenter=0<CR>
nmap <leader>: q:
" }}}

" delimitMate {{{
let delimitMate_expand_space = 1
let delimitMate_expand_cr = 1
let delimitMate_expand_inside_quotes = 0

augroup vimrc_delimitmate_ft
	autocmd!
	" Conflicts with vim-closetag: https://github.com/alvan/vim-closetag/issues/40
	autocmd FileType html let b:delimitMate_matchpairs = "(:),[:],{:}"
	autocmd FileType clojure,lisp let b:delimitMate_quotes = "\""
	autocmd FileType vim let b:delimitMate_quotes = "'"
	autocmd FileType tex let b:delimitMate_quotes = '$'
augroup END
" }}}

" vim-eunuch {{{
let g:eunuch_no_maps = 1  " Conflicts with our delimitMateCR mapping
" }}}

" vim-matchup {{{
let g:matchup_matchparen_deferred = 1
if has('nvim')
	let loaded_matchit = 1  " Comes with nvim
endif
" }}}

" ruanyl/vim-gh-line {{{
let g:gh_line_map = '<Plug>ZZZZVimrcDisabledGHLineMap'
let g:gh_repo_map = '<Plug>ZZZZVimrcDisabledGHLineRepoMap'
let g:gh_line_blame_map = '<Plug>ZZZZVimrcDisabledGHLineBlameMap'
" }}}

" unblevable/quick-scope {{{
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
let g:qs_hi_priority = 100
augroup vimrc_quick_scope
	autocmd!
	autocmd ColorScheme * highlight! QuickScopePrimary gui=bold,underline cterm=bold,underline
	autocmd ColorScheme * highlight! QuickScopeSecondary gui=underline cterm=underline
augroup END
" }}}

" ojroques/vim-oscyank {{{
if !empty($SSH_CONNECTION) || filereadable('/.dockerenv')
	augroup vimrc_vim_oscyank
		autocmd!
		autocmd TextYankPost * if v:event.operator is 'y' && v:event.regname is '+' | execute 'OSCYankRegister +' | endif
	augroup END
endif
" }}}

" vim-unimpaired {{{
nmap -p =p
nmap -P =P
nmap =<leader>p "+=p
nmap =<leader>P "+=P
nmap -<leader>p =<leader>p
nmap -<leader>P =<leader>P
" Use f (in .minivimrc)
noremap ]a <Nop>
noremap [a <Nop>
noremap ]A <Nop>
noremap [A <Nop>
" }}}

" vim-windowswap {{{
let g:windowswap_map_keys = 0
nnoremap <C-w>s <Cmd>call WindowSwap#EasyWindowSwap()<CR>
nnoremap <A-s> <Cmd>call WindowSwap#EasyWindowSwap()<CR>
" }}}

" Konfekt/vim-sentence-chopper {{{
function! s:SetUpSentenceChopperForLatex()
	if &filetype != 'tex' | return | endif
	let l:textwidth = &textwidth > 0 ? &textwidth + 1 : 0
	let b:latexindent_yaml_options = ''
		\ . 'modifyLineBreaks:textWrapOptions:columns: ' . l:textwidth . ','
		\ . 'modifyLineBreaks:textWrapOptions:when: after' . ','
		\ . 'modifyLineBreaks:removeTrailingWhitespace:beforeProcessing: 1' . ','
		\ . 'modifyLineBreaks:removeTrailingWhitespace:afterProcessing: 1' . ','
		\ . 'modifyLineBreaks:oneSentencePerLine:textWrapSentences: 1' . ','
		\ . 'modifyLineBreaks:items:ItemStartsOnOwnLine: 1' . ','
		\ . 'modifyLineBreaks:environments:'
				\ . 'BeginStartsOnOwnLine: 1' . ';'
				\ . 'BodyStartsOnOwnLine: 1' . ';'
				\ . 'EndStartsOnOwnLine: 1' . ';'
				\ . 'EndFinishesWithLineBreak: 1' . ';'
				\ . 'DBSFinishesWithLineBreak: 1' . ','
		\ . 'modifyLineBreaks:specialBeginEnd:displayMath:'
				\ . 'SpecialBeginStartsOnOwnLine: 1' . ';'
				\ . 'SpecialBodyStartsOnOwnLine: 1' . ';'
				\ . 'SpecialEndStartsOnOwnLine: 1' . ';'
				\ . 'SpecialEndFinishesWithLineBreak: 1' . ','
		\ . 'modifyLineBreaks:specialBeginEnd:displayMathTeX:'
				\ . 'SpecialBeginStartsOnOwnLine: 1' . ';'
				\ . 'SpecialBodyStartsOnOwnLine: 1' . ';'
				\ . 'SpecialEndStartsOnOwnLine: 1' . ';'
				\ . 'SpecialEndFinishesWithLineBreak: 1' . ','
endfunction
nmap <leader>w <plug>(ChopSentences)
xmap <leader>w <plug>(ChopSentences)
augroup vimrc_sentence_chopper
	autocmd!
	autocmd BufWinEnter * call s:SetUpSentenceChopperForLatex()
	autocmd OptionSet textwidth,filetype call s:SetUpSentenceChopperForLatex()
augroup END
" }}}

" vim-devicons {{{
let g:webdevicons_enable_airline_statusline = 0
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1

" https://github.com/ryanoasis/vim-devicons/issues/452
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols = {}
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['tex']   = ''  " nf-seti-tex
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['cs']    = '󰌛'  " nf-md-language_csharp
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['r']     = '󰟔'  " nf-md-language_r
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['rproj'] = '󰗆'  " nf-md-vector_rectangle
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['sol']   = '󰡪'  " nf-md-ethereum
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['pem']   = '󰌋'  " nf-md-key_variant
" }}}

" nvim-scrollview {{{
let g:scrollview_signs_on_startup = ['conflicts', 'diagnostics', 'loclist', 'marks', 'quickfix', 'search']
let g:scrollview_signs_show_in_folds = v:true
" let g:scrollview_signs_max_per_row = 3
let g:scrollview_winblend = 35
let g:scrollview_winblend_gui = 35
augroup vimrc_nvim_scrollview
	autocmd!
	autocmd ColorScheme * highlight link ScrollView Pmenu
	autocmd ColorScheme * highlight link ScrollViewMarks FoldColumn
	autocmd ColorScheme * highlight link ScrollViewSearch Search
augroup END
" }}}

" satellite.nvim {{{
augroup vimrc_nvim_satellite
	autocmd!
	autocmd ColorScheme * highlight link SatelliteBar Pmenu
	autocmd ColorScheme * highlight link SatelliteMark Identifier
augroup END
" }}}

if has('nvim-0.5')
	exec 'luafile '.s:script_dir.'/nvim/init.lua'

	" nvim-treesitter
	augroup vimrc_nvim_treesitter
		autocmd!
		for lang in ['c', 'cpp', 'python', 'go']
			execute 'autocmd ColorScheme * highlight link' lang.'error' 'normal'
		endfor
		autocmd FileChangedShellPost * silent! KeepView e  " Fixes treesitter colours
		autocmd ColorScheme * highlight! link @text.note Todo
		autocmd ColorScheme * highlight! link @text.warning Todo
		autocmd ColorScheme * highlight! link @text.danger Todo
		autocmd ColorScheme * highlight! link @comment.note Todo
		autocmd ColorScheme * highlight! link @comment.todo Todo
		autocmd ColorScheme * highlight! link @comment.warning Todo
		autocmd ColorScheme * highlight! link @comment.error Todo
	augroup END

	" rainbow-delimiters.nvim
	let g:rainbow_delimiters = {
		\ 'strategy': {
			\ '': rainbow_delimiters#strategy.global,
		\ },
		\ 'query': {
			\ '': 'rainbow-delimiters',
		\ },
		\ 'highlight': [
			\ 'RainbowDelimiterRed',
			\ 'RainbowDelimiterYellow',
			\ 'RainbowDelimiterBlue',
			\ 'RainbowDelimiterGreen',
		\ ],
	\ }
	augroup vimrc_rainbow_delimiters
		autocmd!
		autocmd BufRead * highlight RainbowDelimiterGreen ctermfg=70 guifg=#5faf00
	augroup END

	" nvim-spectre
	nnoremap GR <cmd>lua require('spectre').open()<CR>
	vnoremap GR <cmd>lua require('spectre').open_visual()<CR>
	nnoremap Gr <cmd>lua require('spectre').open_file_search()<cr>
	vnoremap Gr <cmd>lua require('spectre').open_visual({ path = vim.fn.expand("%") })<CR>

	" gitsigns.nvim
	augroup vimrc_gitsigns_nvim
		autocmd!
		autocmd ColorScheme * highlight link GitSignsAdd GitGutterAdd
		autocmd ColorScheme * highlight link GitSignsDelete GitGutterDelete
		autocmd ColorScheme * highlight GitSignsChange ctermfg=172 guifg=#d78700
	augroup END
	nnoremap <leader>hB <cmd>Gitsigns toggle_current_line_blame<cr>
	nnoremap <leader>ht <cmd>Gitsigns toggle_signs<cr>
endif

" =========================
" Plugin Config Secion Ends
" =========================

" Theming {{{
" Can be overridden by ~/.custom.vimrc
function! g:ApplyThemePatches()
	let g:airline_theme = g:theme != 'dark' ? 'papercolor' : 'tomorrow'
	" https://github.com/NLKNguyen/papercolor-theme
	let g:PaperColor_Theme_Options = {
	\	'theme': {
	\		'default.light': {
	\			'transparent_background': 1,
	\			'allow_bold': 1,
	\			'allow_italic': 1,
	\			'override': {
	\				'color07': ['#1c1c1c', 234],
	\				'cursorline': ['#eeeeee', 255],
	\				'cursorcolumn': ['#eeeeee', 255]
	\			}
	\		},
	\		'default.dark': {
	\			'transparent_background': 1,
	\			'allow_bold': 1,
	\			'allow_italic': 1,
	\			'override': {
	\				'color07': ['#c6c6c6', 251],
	\				'color05': ['#767676', 243]
	\			}
	\		}
	\	}
	\}
endfunction

augroup vimrc_highlight
	autocmd!
	autocmd ColorScheme PaperColor hi! link SignColumn LineNr
	if has('nvim')
		autocmd ColorScheme * if g:theme != 'dark' | highlight! Whitespace ctermfg=254 guifg=#e4e4e4 | endif
		autocmd ColorScheme * if g:theme == 'dark' | highlight! link Whitespace NonText | endif
	endif
augroup END

function! g:ApplyTheme()
	silent! call g:ApplyThemePatches()
	let &background = g:theme == 'dark' ? 'dark' : 'light'
	colorscheme PaperColor
	if has('nvim-0.10')
		highlight Normal ctermbg=none guibg=none
	endif
	syntax on
endfunction

function! g:SwitchTheme()
	let g:theme = g:theme == 'light' ? 'dark' : 'light'
	call g:ApplyTheme()
endfunction
command! SwitchTheme call g:SwitchTheme()
" }}}

if filereadable(expand("~/.custom.vimrc"))
	exec 'source ~/.custom.vimrc'
endif

call g:ApplyTheme()

" vim: set fdm=marker:
