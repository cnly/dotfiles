[Appearance]
AntiAliasFonts=true
ColorScheme=TangoLight
Font=DejaVu Sans Mono,11,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Command=/usr/bin/tmux
Name=myprofile
Parent=FALLBACK/

[Interaction Options]
MouseWheelZoomEnabled=false

[Scrolling]
ScrollBarPosition=2
