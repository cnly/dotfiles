UPDATER := ./update.sh

ifeq (${dotfiles_lightweight_install},)
	INDEPT_COMPONENTS := independent_components_full
else
	INDEPT_COMPONENTS := independent_components_lightweight
endif

.PHONY: all
all: $(INDEPT_COMPONENTS) vim_components

.PHONY: vim_components
vim_components: update_pip_modules
	$(MAKE) update_vim_plugins
	$(MAKE) update_coc_nvim update_nvim_treesitter $([ -n "${dotfiles_lightweight_install}" ] || echo update_vim_go)

.PHONY: independent_components_lightweight
independent_components_lightweight: update_oh_my_zsh update_tmux

.PHONY: independent_components_full
independent_components_full: independent_components_lightweight update_gdb_tools

.DEFAULT:
	$(UPDATER) $@
